<?php
 /*
  * Developed by Cris del Rosario
  */

 require_once 'Session.php';

 class User {
     const STATUS_NEW          = 1;
     const STATUS_ACTIVE       = 2;
     const STATUS_DEACTIVATED  = 3;
     const STATUS_BANNED       = 4;
     const STATUS_CANCELLED    = 5;
     
     const USER_ROLE_UNDEFINED = 0;
     const USER_ROLE_ADMIN     = 1;
     const USER_ROLE_MANAGER   = 2;
     const USER_ROLE_SALES     = 3;
     const USER_ROLE_MEMBER    = 4;
          
     public static function createPhotoAlias($user_id,$username) {
         return md5($user_id . "_" . $username);
     }
     
     public static function encodePass($username,$password) {
         return md5("sphere".$username."rewards".$password."magic");
     }
     
     public static function isAuthenticated($user_id) {
         $state = true;
         if ($user_id == null || $user_id == "") {
             $state = false;
         }
         return $state;
     }
     
     public static function hasRole($role) {
         $role_check = Session::get("role");
         if ($role_check != null && $role_check != "") {
             if ($role_check == $role) {
                 return true;
             }
         }
         return false;
     }
     
    public static function getStatus($status) {
        if ($status == User::STATUS_NEW) {
            return "New";
        }
        else if ($status == User::STATUS_ACTIVE) {
            return "Active";
        }
        else if ($status == User::STATUS_DEACTIVATED) {
            return "Deactivated";
        }
        else if ($status == User::STATUS_BANNED) {
            return "Banned";
        }
        else if ($status == User::STATUS_CANCELLED) {
            return "Cancelled";
        } else {
            return "Unknown";
        }
    }
     
    public static function getRole($role) {
        if ($role == User::USER_ROLE_UNDEFINED) {
            return "Undefined";
        }
        else if ($role == User::USER_ROLE_ADMIN) {
            return "Administrator";
        }
        else if ($role == User::USER_ROLE_MANAGER) {
            return "Manager";
        }
        else if ($role == User::USER_ROLE_SALES) {
            return "Sales";
        }
        else if ($role == User::USER_ROLE_MEMBER) {
            return "Member";
        }
    }
 }
?>