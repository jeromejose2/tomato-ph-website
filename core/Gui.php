<?php
 /*
  * Developed by Cris del Rosario
  */

 require_once 'lib/Twig/Autoloader.php';
 require_once 'Models.php';

 class GUI {
     protected static $twig = null;
	 protected static $loader = null;
	 
     public static function init() {
		Twig_AutoLoader::register();
		
	    self::$loader = new Twig_Loader_Filesystem('ui');
		self::$twig = new Twig_Environment(self::$loader,array());	 
	 }
	 
	 public static function render($template,$data) {
	    $template = self::$twig->loadTemplate($template);
     	$data["root"] = ROOT;
        $template->display($data); 
	 }     
 }
?>