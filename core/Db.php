<?php
 /*
  * Developed by Cris del Rosario
  */

 if ($_SERVER['SERVER_NAME'] == 'localhost') {
     define ('REWARDS_DB_HOST',"localhost");
     define ('REWARDS_DB_USER',"root");
     define ('REWARDS_DB_PASS',"root");
     define ('REWARDS_DB_NAME',"prime_rewards_db");
     
     define ('SPHERE_DB_HOST',"localhost");
     define ('SPHERE_DB_USER',"root");
     define ('SPHERE_DB_PASS',"root");
     define ('SPHERE_DB_NAME',"ls_back");     
 } else {
     define ('REWARDS_DB_HOST',"localhost");
     define ('REWARDS_DB_USER',"ls_mlm_usr");
     define ('REWARDS_DB_PASS',"U77uczvRchjxDh2Qz9C23CCc3");
     define ('REWARDS_DB_NAME',"prime_rewards_db");
     
     define ('SPHERE_DB_HOST',"localhost");
     define ('SPHERE_DB_USER',"ls_mlm_usr");
     define ('SPHERE_DB_PASS',"U77uczvRchjxDh2Qz9C23CCc3");
     define ('SPHERE_DB_NAME',"ls_3962");    

     // define ('REWARDS_DB_HOST',"localhost");
     // define ('REWARDS_DB_USER',"lsprimelogic");
     // define ('REWARDS_DB_PASS',"EyBMaX2GbUa3w658");
     // define ('REWARDS_DB_NAME',"prime_rewards_db");
     
     // define ('SPHERE_DB_HOST',"localhost");
     // define ('SPHERE_DB_USER',"lsprimelogic");
     // define ('SPHERE_DB_PASS',"EyBMaX2GbUa3w658");
     // define ('SPHERE_DB_NAME',"ls_back");     
 }

 define ('USE_SPHERE_DB',1);
 define ('USE_REWARDS_DB',0);

 class Db {
     protected static $dbh;
     protected static $link;
     
     public static function connect($which=USE_REWARDS_DB) {
         $db_host = REWARDS_DB_HOST;
         $db_user = REWARDS_DB_USER;
         $db_pass = REWARDS_DB_PASS;
         $db_name = REWARDS_DB_NAME;

         if ($which == USE_SPHERE_DB) {
             $db_host = SPHERE_DB_HOST;
             $db_user = SPHERE_DB_USER;
             $db_pass = SPHERE_DB_PASS;
             $db_name = SPHERE_DB_NAME;
         }
         
         self::$dbh = mysql_connect($db_host,$db_user,$db_pass);
         if (self::$dbh != null) {
             self::$link = mysql_select_db($db_name);
         }
     }
     
     public static function close() {
         if (self::$dbh) {
             mysql_close(self::$dbh);
         }
         self::$dbh = null;
     }
     
     public static function query($table,$fields,$where,$limit=null,$order=null,$direction=0){
         $data = array();
         $statement = "SELECT ";
         $sep = "";
         
         foreach ($fields as $field) {
             $statement .= $sep . $field;
             $sep = ",";
         }
         
         $statement .= " FROM " . $table;
         if ($where !=  null) {
             $statement .= " WHERE ";
             $sep = "";
         
             foreach ($where as $key => $value) {
                 $statement .= $sep;
                 $statement .= $key . "=" . $value . "";
                 $sep = " AND ";
             }
         }
         
         if ($order != null) {
             $statement .= " ORDER BY ";
             $sep = "";
             foreach ($order as $order_field) {
                 $statement.= $sep;
                 $statement.= $order_field;
                 $sep = ",";
             }
             
             if ($direction == 1) {
                 $statement.= " DESC";
             } else {
                 $statement.= " ASC";
             }
         }         
         
         if ($limit != null) {
             $statement .= " LIMIT " . $limit;
         }
         
         self::connect();
         $result = mysql_query($statement);

         if ($result != null && mysql_num_rows($result) != FALSE) {
             while ($row = mysql_fetch_assoc($result)) {
                 $data[] = $row;
             }
             mysql_free_result($result);
         }
         self::close();
         return $data;
     }
     
     public static function update($table,$fields,$where) {
         $statement = "UPDATE ";
         $statement.= $table;
         $statement.= " SET ";
         $sep = "";
         
         foreach ($fields as $key => $value) {
             $statement.= $sep;
             $statement.= $key. "=" . $value;
             $sep = ",";
         }
         
         $statement.= " WHERE ";
         $sep = "";
         foreach ($where as $key => $value) {
             $statement.= $sep;
             $statement.= $key . "=" . $value;
             $sep = " AND ";
         }
         
         self::connect();
         mysql_query($statement);
         self::close();
     }
     
     public static function execute($statement) {
         $data = array();
         self::connect();
         $result = mysql_query($statement);
         if (count($result) > 0 && !is_bool($result)) {
             if (mysql_num_rows($result) != FALSE) {
                 while ($row = mysql_fetch_assoc($result)) {
                     $data[] = $row;
                 }
                 mysql_free_result($result);
             }
         }
         self::close();
         return $data;
     }
     
     public static function db_execute($where,$statement) {
         $data = array();
         self::connect($where);
         $result = mysql_query($statement);
         if (count($result) > 0 && !is_bool($result)) {
             if (mysql_num_rows($result) != FALSE) {
                 while ($row = mysql_fetch_assoc($result)) {
                     $data[] = $row;
                 }
                 mysql_free_result($result);
             }
         }
         self::close();
         return $data;
     }


     public static function insert($table,$fieldset) {
         $statement = "INSERT INTO " . $table;
         $fields = "";
         $values = "";
         $sep = "";
         foreach ($fieldset as $key => $value) {
             $fields.= $sep;
             $fields.= $key;
             
             $values.= $sep;
             $values.= $value;
             
             $sep = ",";
         }
         
         $statement.= "(" . $fields . ") VALUES (" . $values .")"; 
         echo $statement."<br/>";
         self::connect();
         mysql_query($statement);
         $last_insert_id = mysql_insert_id();
         self::close();
         
         return $last_insert_id;
     }
     
     public static function quote($value) {
         return "'" . mysql_real_escape_string($value) . "'";
     }
 }
?>