<?php
 /*
  * Developed by Cris del Rosario
  */

class Table {
    const USERS = "rewards_users";
    const USER_TYPES = "rewards_user_types";
    const MEMBERS = "rewards_members";
    const DOWNLINES = "rewards_downlines";
    const MEMBER_INCOME = "rewards_member_income";
    const KITS = "rewards_entry_level_kit";
    const PRODUCTS = "rewards_products";
    const PRODUCT_TYPES = "rewards_product_catalog";
    const ACTIVATION_CODES = "rewards_user_activation_codes";
    const SHOPS = "rewards_shops";
    const MEMBER_PURCHASES = "rewards_member_purchases";
    const REWARDS_CONFIG = "rewards_config";
    const ORDERS = "rewards_orders";
    const ORDER_ITEMS = "rewards_order_items";
    const ORDER_INVOICE = "rewards_order_invoice";
    const TERMS_AND_CONDITION = "rewards_terms_and_condition";
    const SHOP_CART_ITEMS = "rewards_shop_cart_items";
	const MESSAGES = "rewards_messages";
    const ANNOUNCEMENTS = "rewards_company_announcement";
	
    
    const SHOP_ORDER_STATUSES = "shop_order_statuses";
    const SHOP_CUSTOMERS = "shop_customers";
}
?>