<?php
 /*
  * Developed by Cris del Rosario
  */

 require_once 'Models.php';

 class Loader {
    private static $model = null;
	
	public static function register() {
        global $MODELS;
	    self::$model = $MODELS;
	}
	
	public static function getModel() {	    
	    return isset($_GET['m']) ? $_GET['m'] : null;
	}
	
	public static function getExtra() {
	    return isset($_GET["extras"]) ? $_GET["extras"] : null;
	}
	
	public static function setPage($page) {	    
	    header("Location: " + $page);
		exit(1);
	}
    	 
	public static function bootup() {
	    $mod = self::getModel();		
		$module = null;
		if ($mod != null) {
            $modt = preg_replace("/([-]+)/","_",$mod);
            if ($modt != null) {
                $mod = $modt;
            }
            
            $model = "home";
            if (preg_match("/^(\w+)\/.+$/sm",$mod,$map)) {
                $model = $map[1];
            }
            
            if (in_array($model,array("home","help","brands","recruitment","sphere","members","admin","manager","sales","api"))) {
                foreach (self::$model[$model] as $page) {
                    if (strcasecmp($mod,$page) == 0) {
                        $m = explode("/",$page);
                        $path = "";
                        if ($m == false) {
                            $class_name = $page;
                        } else {
                            if (count($m) > 1) {
                                $class_name = end($m);
                                unset($m[count($m)-1]);
                                $path = join("/",$m);
                                $path.= "/";                            
                            } else {
                                //echo $page;
                                $class_name = $page;
                            }
                        }
                        //print 'model/'.$path.$class_name;
                        require_once 'model/' .$path . $class_name . '.php';
                        $module = new $class_name();
                        break;
                    }
                }        
            }            
		}
		
		if ($module != null) {
		    $module->create(self::getExtra());
			$module->render();
		} else {
		    if ($mod == null) {			    
			    self::setPage("home");
			} else {
				print "<strong>Error:</strong> The View was not found! Please see the description below.<br>\n";
				print "<strong>Reason:</strong> " . $mod;			
			}
		}
	}
 }
?>