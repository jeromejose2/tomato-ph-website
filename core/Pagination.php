<?php
 /*
  * Developed by Cris del Rosario
  */

 class Pagination {
     public static function calc($page,$range,$total,$items) {
         $pages = array();
         
         $total_pages = ceil($total / $items);
                 
         $n = (($page-1)-(($page-1)%$range))+1;
         $n = $n > $total_pages ? $total_pages - $range : $n;
         
         $goal = ceil((($n + $range)/$range) + 1);
         
         $n = $n < 1 ? 1 : $n;
         $goal = $goal < $range ? $range : $goal;
         $goal += $n;
         
         $prev = $n-1;
         if ($prev < 1) {
             $prev = 1;
         }

         while ($n < $goal) {
             $pages[] = array("id" => $n,"show" => $n > $total_pages ? 0 : 1);
             $n++;
         }
         
         $next = $n;
         if ($next > $total_pages) {
             $next = $total_pages;
         }
         
         return array(
             "first" => 1,
             "prev" => $prev,
             "next" => $next,
             "last" => $total_pages,
             "total" => $total_pages,
             "pages" => $pages);
     }
 }
?>