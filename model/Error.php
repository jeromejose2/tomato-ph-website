<?php
 require_once 'core/Model.php';
 
 class Error extends Model {
    public function __construct() {
        parent::__construct($this);
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);
	}
	
	public function render() {
		$extras = parent::getExtras();
        
        $error_code = 0;
        $error_message = "";
        if ($extras != NULL && count($extras) > 0) {        
            if (isset($extras["code"])) {
                $error_code = intval($extras["code"]);
            }
        }
        if ($error_code == 101) {
            $error_message = "We could not process your request. The email address you specified is already in use.";
        } else if($error_code == 102){
            $error_message = "We could not process your request. Activation Code is Invalid ";
        }else {
            $error_message = "Unknown Error";
        }
        GUI::render("error.tpl.php",array("pageTitle" => "Error",
                                          "root" => ROOT,
                                          "error_message" => $error_message
                                    ));
	}	
 }
?>