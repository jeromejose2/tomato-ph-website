<?php
 require_once 'core/Model.php';
 
 class Verify_Account extends Model {
    public function __construct() {
        parent::__construct($this);
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);
	}
	
	public function render() {
        $info = array('pageTitle' => 'Verify Account',
                     'root' => ROOT);

        GUI::render("verify_account.tpl.php",$info);
    }
 }
?>