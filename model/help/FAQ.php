<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 
 class FAQ extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
		GUI::render("home.tpl.php",array('pageTitle' => 'FAQ',
                                         'root' => '../'));
	}
 }
?>