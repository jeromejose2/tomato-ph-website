<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 
 class Terms_And_Condition extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        $terms_and_condition = "";
        $result = Db::query(Table::TERMS_AND_CONDITION,array("content"),null,"0,1");
        if (count($result) > 0) {
            $terms_and_condition = $result[0]["content"];
        }
		GUI::render("help/terms_and_condition.tpl.php",array("pageTitle" => "Terms & Condition",
                                        "root" => ROOT,
                                        "terms_and_condition" => $terms_and_condition));
	}
 }
?>