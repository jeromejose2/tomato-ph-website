<?php
 require_once 'core/Model.php';
 
 class SignIn extends Model {
    public function __construct() {
        parent::__construct($this);
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);
	}
	
	public function render() {
        $username = parent::getPost("user");
        $password = parent::getPost("pass");
        
        // echo $username . ' - '. $password . '</br>';
        // echo User::encodePass($username,$password);
        //echo md5("sphere".$username."rewards".$password."magic") . ' </br>';

        // //$option = array();
        // if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
        //     $option = array("email" => Db::quote($username));
        // } else {
        //     $option = array("user" => Db::quote($username));
        // }
        
        
        $result = Db::db_execute(USE_SPHERE_DB,"SELECT * FROM shop_customers a WHERE a.email = '$username'");
        // print_r($result);
        if ( count($result) > 0 ){
             // $user_home = "members/home";
            Session::start();
            Session::register("user_id2",$result[0]["id"]);
            Session::register("role",4);
            $user_id2 = $result[0]["id"];    
            //query here
            $result_ =Db::query(Table::USERS,array("id","email"),
                                        array("email"=>"'".$username."'"),"0,1");       
            //echo $result_[0]['email'];
            Session::register("user_id",$result_[0]["id"]);

            Db::update(Table::USERS,array("login_attempt_count" => "0"),array("id" => $result[0]["id"]));
            Db::execute("INSERT INTO rewards_login_history(login_date,user_id) VALUES(NOW(),'$user_id2')");
            //echo "test 1 </br>";
            parent::redirectTo(ROOT . "members/home");
        }else{
            //echo "test 2 </br>";
            parent::redirectTo(ROOT . "home");
        }
       //  //print_r( $result ); 
       // echo $username . '</br>';
        $result = Db::query(Table::USERS,array("id","user","pass","role","status","login_attempt_count","notify_password_change"),
                                        array("email"=>"'".$username."'"),"0,1");       
        //return md5("sphere".$username."rewards".$password."magic");
        //echo "here 1";
        if (count($result) > 0) {
            $result = $result[0];
            
            if ($result["notify_password_change"] == 1) {
            }
            
            if (intval($result["login_attempt_count"]) > 4) {
                //echo "1";
                parent::redirectTo(ROOT . "home");
                 //echo Session::get("user_id");
            } else {
                if ($result["pass"]==User::encodePass($username,$password)) {

                    if ($result["role"] == User::USER_ROLE_ADMIN || $result["role"] == User::USER_ROLE_MEMBER || $result["role"] == User::USER_ROLE_MANAGER || $result["role"] == User::USER_ROLE_SALES) {
                        if ($result["status"] == User::STATUS_ACTIVE) {
                           
                            Session::start();
                            Session::register("user_id",$result["id"]);
                            Session::register("role",$result["role"]);
                            
                            $user_id = $result["id"];
                            
                            Db::update(Table::USERS,array("login_attempt_count" => "0"),array("id" => $result["id"]));
                            Db::execute("INSERT INTO rewards_login_history(login_date,user_id) VALUES(NOW(),'$user_id')");
                            
                            $user_home = "home";
                            if ($result["role"] == User::USER_ROLE_ADMIN) {
                                $user_home = "admin/home";
                            } else if ($result["role"] == User::USER_ROLE_MANAGER) {
                                $user_home = "manager/home";
                            } else if ($result["role"] == User::USER_ROLE_SALES) {
                                $user_home = "sales/home";
                            } else if ($result["role"] == User::USER_ROLE_MEMBER) {
                                $user_home = "members/home";
                            }
                            //echo "2";
                            parent::redirectTo(ROOT . $user_home);
                        } else {
                           // echo "here 2";
                            if ($result["role"] == User::USER_ROLE_MEMBER && $result["status"] == User::STATUS_NEW) {
                                Session::start();
                                Session::register("user_id",$result["id"]);
                                Session::register("role",$result["role"]);
                                //echo "3";
                                parent::redirectTo(ROOT . "activate");
                            } else {
                                //echo "4";
                                parent::redirectTo(ROOT . "home");
                            }
                        }
                    } else {
                         //echo "5";
                        parent::redirectTo(ROOT . "home");
                    }
                } else {

                    // $result_ =Db::query(Table::USERS,array("email"),
                    //                     array("email"=>"'".$username."'"),"0,1");    
                    Db::update(Table::USERS,array("login_attempt_count"=>"(login_attempt_count + 1)"),array("id" => $result["id"]));
                    //echo "6 </br>";
                    parent::redirectTo(ROOT . "home");
                   

                }
            }
        } else {
            parent::redirectTo(ROOT . "home");
        }
		echo "asdf".Session::get("user_id");
	}	
}
?>