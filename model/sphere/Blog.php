<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 
 class Blog extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
		GUI::render("home.tpl.php",array('pageTitle' => 'Blog',
                                         'root' => '../'));
	}
 }
?>