<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 
 class Signup_Select_Product extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        
        $info = array('pageTitle' => 'Select Product',
                      'root' => ROOT);
        
        $member_id = Session::get("member_id");
        
        $result = Db::query(Table::PRODUCTS,array("id","name","price","description","photo"),null,"0,10");
        $products = array();
        if (count($result) > 0) {
            foreach ($result as $product) {
                $products[] = array(
                    "id" => $product["id"],
                    "photo" => $product["photo"],
                    "name" => $product["name"],
                    "price" => $product["price"],
                    "description" => $product["description"]
                );
            }
        }
        
        $info["products"] = $products;
        $info["member_id"] = $member_id;
        
		GUI::render(ROOT . "signup_select_product.tpl.php",$info);
	}
 }
?>