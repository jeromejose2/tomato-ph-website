<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 
 class Purchase_Kit extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        
        $extras = self::getExtras();
        
        $kit_price = 0;
        if ($extras[1] == "price") {
            $kit_price = $extras[2];
        }        
        
        $member_id = Session::get("member_id");
                
        $sp = 0.2;
        $MR = 170;
        
        $sponsor_id = 0;
        
        Db::update(Table::MEMBERS,array("points" => (($kit_price*$sp)*0.10)),array("id" => $member_id),null,null);
                    
        Db::insert(Table::MEMBER_INCOME,array("member_id" => $member_id,
                                              "date" => "NOW()",
                                              "points" => (($kit_price*$sp)*0.10),
                                              "price" => $kit_price,
                                              "product_type" => 1,
                                              "product_id" => 1,
                                              "ref_id" => $member_id,
                                              "discount" => 0.10),null,null);
        $result = Db::query(Table::MEMBERS,array("sponsor"),array("id" => $member_id),"0,1");
        if (count($result) > 0) {
            $result = $result[0];
            $sponsor_id = $result["sponsor"];
            
//            Db::insert(Table::DOWNLINES,array("parent" => $sponsor_id,
//                                              "child" => $member_id));
            
            Db::nsert(Tabe::MEMBER_PURCHASES,array("member_id" => $member_id,"price" => $kit_price,"purchase_date" => "NOW()","product_id" => "1"));
//            $sponsor_idx = 0;
//            while ($sponsor_idx < 5) {
//                $result = Db::query(Table::MEMBERS,array("points","sponsor"),array("id" => $sponsor_id),"0,1");
//                if (count($result)) {
//                    $points = $result[0]["points"];
//                    if ($points >= $MR) {
//                        Db::update(Table::MEMBERS,array("points" => "points + " . (($kit_price*$sp)*0.3)),array("id"=>$sponsor_id));
//                        Db::insert(Table::MEMBER_INCOME,array("member_id" => $sponsor_id,
//                                                              "date" => "NOW()",
//                                                              "points" => (($kit_price*$sp)*0.3),
//                                                              "price" => $kit_price,
//                                                              "product_type" => 1,
//                                                              "product_id" => 1,
//                                                              "ref_id" => $member_id,                                                          
//                                                              "discount" => 0.3),null,null);
//                        $sponsor_idx++;
//                    }                
//                    $sponsor_id = $result[0]["sponsor"];
//                } else {
//                    break;
//                }            
//            }            
        }
                    
        parent::redirectTo(ROOT . "signup-select-product");
	}
 }
?>