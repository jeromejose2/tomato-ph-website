<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 
 class Signup_Select_Kit extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        $info = array('pageTitle' => 'Select Kit',
                      'root' => ROOT);
        Session::start();
        $member_id = Session::get("member_id");
        
        $result = Db::query(Table::PRODUCTS,array("id","name","price","description","photo"),array("product_type" => "1"),"0,10");
        $kits = array();
        if (count($result) > 0) {
            foreach ($result as $kit) {
                $kits[] = array(
                    "id" => $kit["id"],
                    "photo" => $kit["photo"],
                    "name" => $kit["name"],
                    "price" => $kit["price"],
                    "description" => $kit["description"]
                );
            }
        }
        
        $result = Db::query(Table::PRODUCT_TYPES,array("title"),array("id" => "1"),"0,10");
        if (count($result) > 0) {
            $info["category"] = $result[0]["title"];
        }
        
        $info["kits"] = $kits;
        
        Session::register("member_id",$member_id);
        
		GUI::render("signup_select_kit.tpl.php",$info);
	}
 }
?>