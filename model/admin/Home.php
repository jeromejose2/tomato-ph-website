<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 
 class Home extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        //echo "render";
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN)) {
            $info = array(
                "pageTitle" => "Home",
                "root" => ROOT,
                "name" => "",
                "last_login" => ""
            );
            
            $result = Db::query(Table::USERS,
                                array("id","fullname"),
                                array("id"=>$user_id),"0,1");        
            if (count($result) > 0) {
                $result = $result[0];                      
                $info["name"] = $result["fullname"];
            }
            
            $login_result = Db::execute("SELECT DATE_FORMAT(login_date,'%W, %M %e, %Y @ %h:%i %p') AS lastlogin FROM rewards_login_history LIMIT 0,2");
            $info["last_login"] = date("F j, Y, g:i a");
            if (count($login_result) > 1) {
                $info["last_login"] = $login_result[1]["lastlogin"];
            }
            
            $members = array();
            $result = Db::query(Table::MEMBERS,array("firstname","lastname","sponsor","creation_date"),null,"0,5",array("id"),1);
            $itemCount = 0;
            if (count($result) > 0) {
                foreach ($result as $item) {
                    $members[] = array(
                        "name" => $item["firstname"] . " " . $item["lastname"],
                        "sponsor" => self::getSponsor($item["sponsor"]),
                        "date" => $item["creation_date"]
                    );
                    $itemCount++;
                }
            
            }
            
            while ($itemCount < 5) {
                $members[] = array(
                    "name" => "",
                    "sponsor" => "",
                    "date" => ""
                );
                $itemCount++;
            }
            
            $result = Db::query(Table::USERS,array("COUNT(id) num_users"),null,null);
            if (count($result) > 0) {
                $info["num_users"] = $result[0]["num_users"];
            }
            
            $result = Db::query(Table::USERS,array("COUNT(id) num_users"),array("status" => User::STATUS_NEW),null);
            if (count($result) > 0) {
                $info["new_users"] = $result[0]["num_users"];
            }            
            
            $result = Db::query(Table::MEMBERS,array("COUNT(id) num_members"),null,null);
            if (count($result) > 0) {
                $info["num_members"] = $result[0]["num_members"];
            }
            $result = Db::query(Table::MEMBERS,array("COUNT(id) num_members"),array("status" => User::STATUS_NEW),null);
            if (count($result) > 0) {
                $info["new_members"] = $result[0]["num_members"];
            }
            
            $result = Db::query(Table::MEMBERS,array("COUNT(id) num_members"),array("activated" => 0),null);
            if (count($result) > 0) {
                $info["pending_members"] = $result[0]["num_members"];
            }
            
            $info["members"] = $members;
            
            self::displayGraph();
            
            GUI::render("admin/home.tpl.php",$info);            
        } else {
            parent::redirectTo(ROOT . "logout");
        }
	}
     
    private function displayGraph() {
//        require_once 'lib/jpgraph/jpgraph.php';
//        require_once 'lib/jpgraph/jpgraph_line.php';
//        require_once 'lib/jpgraph/jpgraph_pie.php';
//        require_once 'lib/jpgraph/jpgraph_pie3d.php';
//        require_once 'lib/jpgraph/jpgraph_bar.php';
//
//        $ydata = array(11,3,8,12,5,1,9,13,5,7);
//        
//        $width=650;
//        $height=250;
//        
//        $graph = new Graph($width,$height);
//        $graph->SetScale('intlin');
//        
//        $graph->SetMargin(40,20,20,40);
//        $graph->title->Set('Members per Month');
//        $graph->subtitle->Set('(March 12, 2008)');
//        $graph->xaxis->title->Set('Operator');
//        $graph->yaxis->title->Set('# of calls');
//        
//        $lineplot=new LinePlot($ydata);
//        
//        $graph->Add($lineplot);
//        
//        $graph->Stroke('images/members-line.png');    
//        
//        // pie graph
//        $data = array(40,60,21,33);
//
//        $graph = new PieGraph(300,200);
//        $graph->SetShadow();
//        
//        $graph->title->Set("A simple Pie plot");
//        $graph->title->SetFont(FF_FONT1,FS_BOLD);
//        
//        $p1 = new PiePlot3D($data);
//        $p1->SetSize(0.5);
//        $p1->SetCenter(0.45);
//        $p1->SetLegends($gDateLocale->GetShortMonth());
//        
//        $graph->Add($p1);
//        $graph->Stroke('images/members-pie.png');
//        
//        // bar plot
//        $datay=array(12,8,19,3,10,5);
//        
//        // Create the graph. These two calls are always required
//        $graph = new Graph(300,200);
//        $graph->SetScale('textlin');
//        
//        // Add a drop shadow
//        $graph->SetShadow();
//        
//        // Adjust the margin a bit to make more room for titles
//        $graph->img->SetMargin(40,30,40,40);
//        
//        // Create a bar pot
//        $bplot = new BarPlot($datay);
//        $graph->Add($bplot);
//        
//        // Create and add a new text
//        $txt=new Text('This is a text');
//        $txt->SetPos(0,20);
//        $txt->SetColor('darkred');
//        $txt->SetFont(FF_FONT2,FS_BOLD);
//        $graph->AddText($txt);
//        
//        // Setup the titles
//        $graph->title->Set("A simple bar graph");
//        $graph->xaxis->title->Set("X-title");
//        $graph->yaxis->title->Set("Y-title");
//        
//        $graph->title->SetFont(FF_FONT1,FS_BOLD);
//        $graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
//        $graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);
//        
//        // Display the graph
//        $graph->Stroke('images/members-bar.png');
//        
    }
     
    private function getSponsor($sponsor) {
        $result = Db::query(Table::MEMBERS,array("firstname","lastname"),array("id" => $sponsor),"0,1");
        if (count($result) > 0) {
            return $result[0]["firstname"] . " ". $result[0]["lastname"];
        } else {
            return "";
        }
    }    
    private function status(){
        echo "This";
    } 
 }
?>