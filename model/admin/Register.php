<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 require_once 'core/Pagination.php';
 
 class Register extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN)) {
            if (parent::isPostRequest()) {
                $kit = self::getKit();
                GUI::render("admin_register_step2.tpl.php",array('pageTitle' => 'Registers',
                                                 'show_cart_items' => 'true',
                                                 'root' => '../../../',
                                                 'kit' => $kit
                                                ));
            } else {
                list ($year,$day) = self::createDate();
                $kit = self::getKit();
                $products = self::getProducts();
                
                GUI::render("admin_register.tpl.php",array('pageTitle' => 'Registers',
                                                 'show_cart_items' => 'true',
                                                 'root' => '../',
                                                 'year' => $year,
                                                 'day' => $day,
                                                 'kit' => $kit,
                                                 'products' => $products
                                                ));
            }
        } else {
            parent::redirectTo("../logout");
        }
	}
     
    private function getKit() {
        $kit = array();
        $result = Db::query(Table::KIT,array("image","name","description","price"),null,null);
        if (count($result) > 0) {
            foreach ($result as $item) {
                $kit[] = array(
                    "name" => $item["name"],
                    "image" => $item["image"],
                    "description" => $item["description"],
                    "price" => $item["price"]
                );
            }
        }
        return $kit;
    }
     
    private function getProducts() {
        $products = array();
        $result = Db::query(Table::PRODUCTS,
                              array("product_id","name","description","price","status","image","date_added"),
                              array("status"=>"active"),null);
        if (count($result) > 0) {
            foreach ($result as $product) {
                $products[]=array(
                    "product_id" => $product["product_id"],
                    "name" => $product["name"],
                    "description" => $product["description"],
                    "price" => $product["price"],
                    "status" => $product["status"],
                    "image" => $product["image"],
                    "date_added" => $product["date_added"]
                );
            }
        }
        return $products;
    }
     
    private function createDate() {
        $year = array();
        $day = array();
        
        $n = intval(date("Y"));
        $g = $n - 100;
        while ($n >= $g) {
            $year[] = $n;
            $n--;
        }
        
        $n = 1;
        $g = 31;
        while ($n <= $g) {
            $day[] = $n;
            $n++;
        }
        return array($year,$day);        
    }
 }
?>