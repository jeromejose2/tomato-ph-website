<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 require_once 'core/Pagination.php';
 
 class Add_User_Success extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN)) {
            $info = array(
                "pageTitle" => "Add User Sucess",
                "root" => ROOT,
            );            
            GUI::render("admin/add_user_success.tpl.php",$info);
        } else {
            parent::redirectTo(ROOT . "logout");
        }
	}
 }
?>