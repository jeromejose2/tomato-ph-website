<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 require_once 'core/Pagination.php';
 
 class Add_Product extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN)) {
            $info = array(
                "pageTitle" => "Add Product",
                "root" => ROOT,
            );
            if (parent::isPostRequest()) {
                $name = parent::getPost("name");
                $description = parent::getPost("description");
                $price = floatval(parent::getPost("price"));
                $discount = floatval(parent::getPost("discount"));
                $quantity = intval(parent::getPost("quantity"));
                $product_type = intval(parent::getPost("product_type"));
                $photo = "";
                
                $file = $_FILES["photo"];
                if (in_array($file["type"],array("image/jpeg","image/jpg","image/pjpeg","image/x-png","image/png"))) {
                    $fileSize = intval($file["size"]);
                    $fileExt = end(explode(".",$file["name"]));
                    if (in_array($fileExt,array("jpg","jpeg","png")) && $fileSize < 20000) {
                        if ($file["error"] > 0) {
                        } else {
                            $photo = "images/" . md5($user_id . date("Ymd")) . "." . $fileExt;
                            move_uploaded_file($file["tmp_name"],$filePath);
                        }
                    } else {
                    }
                }
                
                Db::insert(Table::PRODUCTS,array("in_stock" => $quantity,
                                                 "price" => $price,
                                                 "type" => $product_type,
                                                 "description" => Db::quote($description),
                                                 "name" => Db::quote($name),
                                                 "photo" => Db::quote($photo),
                                                 "discount" => $discount));
            } else {
                $result = Db::query(Table::PRODUCT_TYPES,array("id","title  "),null,null);
                if (count($result) > 0) {
                    $info["product_types"] = $result;    
                }
                GUI::render("admin/add_new_product.tpl.php",$info);
            }
        } else {
            parent::redirectTo(ROOT . "logout");
        }
	}
 }
?>