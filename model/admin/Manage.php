<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 require_once 'core/Pagination.php';
 
 class Manage extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN)) {
            $info = array(
                "pageTitle" => "Manage Users",
                "root" => ROOT
            );
            
            $extras = parent::getExtras();
            if (count($extras) >= 1 && isset($extras["action"]) && in_array($extras["action"],array("search","ban","view","delete","addnew","search"))) {
                $id = isset($extras["id"]) ? intval($extras["id"]) : 0;
                if ($extras["action"] == "view") {
                    $result = Db::execute("SELECT ru.id,ru.user,rt.name AS role,ru.fullname,ru.email,rs.name AS status,DATE_FORMAT(ru.creation_date,'%W, %M %e, %Y @ %h:%i %p') AS creation_date,ru.created_by FROM rewards_users AS ru
INNER JOIN rewards_user_status AS rs ON ru.status = rs.id INNER JOIN rewards_user_types AS rt ON ru.role = rt.id WHERE ru.id='$id' LIMIT 0,1");
                    if (count($result) > 0) {
                        $result = $result[0];
                        $info["id"] = $result["id"];
                        $info["user"] = $result["user"];
                        $info["role"] = $result["role"];
                        $info["fullname"] = $result["fullname"];
                        $info["email"] = $result["email"];
                        $info["status"] = $result["status"];
                        $info["creation_date"] = $result["creation_date"];
                        $info["created_by"] = self::getUser($result["created_by"]);
                    }
                    GUI::render("admin/manage_view_detail.tpl.php",$info);
                } else if ($extras["action"] == "ban") {
                    if ($id != $user_id) {
                        Db::update(Table::USERS,array("status" => User::STATUS_BANNED),array("id" => $id));
                    }
                    parent::redirectTo(ROOT . "admin/manage");
                } else if ($extras["action"] == "delete") {
                    if ($id != $user_id) {
                        Db::update(Table::USERS,array("status" => User::STATUS_CANCELLED),array("id" => $id));
                    }
                    parent::redirectTo(ROOT . "admin/manage");
                } else if ($extras["action"] == "addnew") {
                    self::displayAddNewUserForm();
                } else if ($extras["action"] == "search") { 
                    self::displaySearchUserForm();
                } else {
                    parent::redirectTo(ROOT . "admin/manage");
                }
            } else {
                $num_users = 0;
                $result = Db::execute("SELECT COUNT(id) AS num_users FROM rewards_users");
                if (count ($result) > 0) {
                    $num_users = intval($result[0]["num_users"]);
                }
                
                $page = 1;
                $user_type = 0;
            
                $extras = parent::getExtras();
                if (count($extras) > 0) {
                    if (isset($extras["page"])) {
                        $page = intval($extras["page"]);
                        $page = $page < 1 ? 1 : $page;
                    } 
                    
                    if (isset($extras["user_type"])) {
                        $user_type = intval($extras["user_type"]);
                        if ($user_type < 0) {
                            $user_type = 0;
                        }
                    }
                }
                
                $limit = 10;
                $offset = $limit * ($page-1);
                            
                $pages = Pagination::calc($page,5,$num_users,$limit);
            
                $info["num_pages"] = $pages["total"];
                $info["current_page"] = $page;
                $info["pages"] = $pages["pages"];
                $info["first"] = $pages["first"];
                $info["prev"] = $pages["prev"];
                $info["next"] = $pages["next"];
                $info["last"] = $pages["last"];
                
                $where = "";
                if ($user_type != 0) {
                    $where = "WHERE ru.role = '$user_type'";
                }
                
                $result = Db::execute("SELECT ru.id,ru.user,ru.role,ru.fullname,ru.created_by,ru.creation_date,rs.name AS status FROM rewards_users AS ru INNER JOIN rewards_user_status AS rs ON ru.status = rs.id INNER JOIN rewards_user_types AS rt ON ru.role = rt.id $where ORDER BY ru.fullname,ru.creation_date DESC LIMIT $offset,$limit");
                $itemCount = 0;
    
                if (count($result) > 0) {
                    $users = array();
                    foreach ($result as $item) {
                        $creator = self::getUser($item["created_by"]);
                        $users[] = array(
                            "id" => $item["id"],
                            "name" => $item["fullname"],
                            "user" => $item["user"],
                            "role" => $item["role"],
                            "creator" => $creator,
                            "date" => $item["creation_date"],
                            "status" => $item["status"]
                        );
                        $itemCount++;
                    }
                }
                
                while ($itemCount < 10) {
                    $users[] = array(
                        "id" => "",
                        "name" => "",
                        "user" => "",
                        "role" => "",
                        "creator" => "",
                        "date" => "",
                        "status" => ""
                    );
                    $itemCount++;                
                }
                            
                $info["users"] = $users;
                $info["user_types"] = self::getUserTypes();
                $info["user_type"] = $user_type;
            
                GUI::render("admin/manage.tpl.php",$info);
            }
            
        } else {
            parent::redirectTo(ROOT . "logout");
        }
	}
     
    private function displayAddNewUserForm() {
        $info = array(
                "pageTitle" => "Add New User",
                "root" => ROOT
        );
        GUI::render("admin/addnewuser.tpl.php",$info);
    }
     
    private function displaySearchUserForm() {
        $info = array(
                "pageTitle" => "Search User",
                "root" => ROOT
        );
        GUI::render("admin/searchuser.tpl.php",$info);
    }
    
    private function getUserTypes() {
        $user_types = Db::query(Table::USER_TYPES,array("id","name"),null,null);
        return $user_types;
    }
    
    private function getUser($user) {
        $result = Db::query(Table::USERS,array("user"),array("id" => $user),"0,1");
        if (count($result) > 0) {
            return $result[0]["user"];
        } else {
            return "System";
        }
    }
 }
?>