<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 require_once 'core/Pagination.php';
 
 class Sphere_Reports extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN)) {
            $info = array(
                "pageTitle" => "Sphere Reports",
                "root" => ROOT,
            );           
            
            $info["num_pages"] = 1;
            $info["current_page"] = 1;
                
            GUI::render("admin/sphere_reports.tpl.php",$info);
        } else {
            parent::redirectTo(ROOT . "logout");
        }
	}
 }
?>