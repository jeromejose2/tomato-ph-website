<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 require_once 'core/Pagination.php';
 
 class Account_Settings extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN)) {
            $supressDisplay = false;
            $extras = parent::getExtras();
            if ($extras != null && count($extras) > 0) {
                $action = isset($extras["action"]) ? $extras["action"] : "";
                if ($action == "changepass") {
                    if (parent::isPostRequest()) {
                        $result = self::updatePass($user_id);
                        if (!$result) {
                            self::displayChangePassResult("Success","Password has been changed successfully");                            
                        } else if ($result == 1) {
                            self::displayChangePassResult("Error","Password does not match the confirm password");                        
                        } else if ($result == 2) {
                            self::displayChangePassResult("Error","Old password does not match current password");
                        }
                    } else {
                        self::displayChangePass();
                    }
                    $supressDisplay = true;
                } else if ($action == "updateinfo") {
                    if (parent::isPostRequest()) {
                        self::updateUserInfo($user_id);
                        self::displayUpdateInfoResult("Success","Info has been updated successfully");
                    } else {
                        self::displayUpdateInfo($user_id);
                    }
                    $supressDisplay = true;
                }
            }
            if (!$supressDisplay) {
                self::displaySettings();
            }
        } else {
            parent::redirectTo(ROOT . "logout");
        }
	}
     
    public function displayUpdateInfoResult($title,$message) {
        $info = array("pageTitle" => $title,
                      "message" => $message);
        GUI::render("admin/update_info_result.tpl.php",$info);        
    } 
     
    public function updateUserInfo($user_id) {
        $fullname = Db::quote(parent::getPost("fullname"));
        $email = Db::quote(parent::getPost("email"));
        
        Db::execute("UPDATE `rewards_users` SET fullname=$fullname, email=$email WHERE id='$user_id'");
    } 
     
    public function displayUpdateInfo($user_id) {
        $info = array("pageTitle" => "Update Info");
        
        $result = Db::execute("SELECT ru.`fullname`,ru.`email` FROM `rewards_users` AS ru WHERE ru.`id` = '$user_id' LIMIT 0,1");
        if (count($result) > 0) {
            $result = $result[0];
            
            $info["fullname"] = $result["fullname"];
            $info["email"] = $result["email"];
        }
        
        GUI::render("admin/update_info.tpl.php",$info);
    } 
     
    public function displayChangePass() {
        $info = array("pageTitle" => "Change Password");
        GUI::render("admin/change_password.tpl.php",$info);
    }
     
    public function displayChangePassResult($title,$message) {
        $info = array("pageTitle" => $title,
                      "message" => $message);
        GUI::render("admin/change_password_result.tpl.php",$info);       
    } 
    
    public function updatePass($user_id) {
        $oldpass = parent::getPost("oldpass");
        $newpass = parent::getPost("newpass");
        $confirmpass = parent::getPost("confirmpass");
        
        $pass = "";
        $user = "";
        
        $state = 0;
        
        $result = Db::execute("SELECT ru.`user`, ru.`pass` FROM `rewards_users` AS ru WHERE ru.`id` = '$user_id'");
        if (count($result) > 0) {
            $result = $result[0];
            $pass = $result["pass"];
            $user = $result["user"];
        }
        
        if ($newpass != $confirmpass) {
            $state = 1;
        } else {
            $oldpass = User::encodePass($user,$oldpass);
            if ($pass != $oldpass) {
                $state = 2;
            } else {
                $newpass = User::encodePass($user,$newpass);
                $user_id = Session::get("user_id");
                Db::execute("UPDATE rewards_users SET pass=" . Db::quote($newpass) . " WHERE id='" . $user_id . "'");
            }
        }
        
        return $state;
    } 
     
    public function displaySettings() {
        $info = array(
            "pageTitle" => "Account Settings",
            "root" => ROOT,
        );
        
        GUI::render("admin/account_settings.tpl.php",$info);
    }
 }
?>