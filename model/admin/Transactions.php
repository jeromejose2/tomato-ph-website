<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 require_once 'core/Pagination.php';
 
 class Transactions extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN)) {
            $transactions = self::getTransactions();
            GUI::render("admin_transactions.tpl.php",array('pageTitle' => 'Transactions',
                                             'show_cart_items' => 'true',
                                             'root' => '../',
                                             'transactions' => $transactions
                                             ));
        } else {
            parent::redirectTo("../logout");
        }
	}
     
    private function getTransactions() {
        $transactions = array();
        $result = Db::query(Table::TRANSACTIONS,array("transaction_id","totalcost","transaction_date","buyer"),null,null);
        if (count($result) > 0) {
            foreach ($result as $t) {
                $transactions[]=array(
                    "transaction_id" => $t["transaction_id"],
                    "totalcost" => $t["totalcost"],
                    "transaction_date" => $t["transaction_date"],
                    "buyer" => $t["buyer"]
                );
            }
        }
        return $transactions;
    }
 }
?>