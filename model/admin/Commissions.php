<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 require_once 'core/Pagination.php';
 
 class Commissions extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN)) {
            $info = array(
                "pageTitle" => "Commissions",
                "root" => ROOT,
            );
            
            $num_commissions = 0;
            
            $result = Db::execute("SELECT COUNT(rmc.id) AS num_commissions FROM rewards_member_commissions AS rmc INNER JOIN rewards_members AS rm ON rmc.member_id = rm.id");
            if (count($result) > 0) {
                $num_commissions = $result[0]["num_commissions"];
            }
            $num_pages = 1;
            $page = 1;
            
            $extras = parent::getExtras();
            if (count($extras) > 0) {
                if (isset($extras["page"])) {
                     $page = intval($extras["page"]);
                    $page = $page < 1 ? 1 : $page;
                } 
            }
            
            $limit = 10;
            $offset = $limit * ($page-1);
            
            $pages = Pagination::calc($page,5,$num_commissions,$limit);
            
            $info["num_pages"] = $pages["total"];
            $info["current_page"] = $page;
            $info["pages"] = $pages["pages"];
            $info["first"] = $pages["first"];
            $info["prev"] = $pages["prev"];
            $info["next"] = $pages["next"];
            $info["last"] = $pages["last"];
            
            $commissions = array();
            $item_count = 0;
            $members = Db::execute("SELECT rmc.start_date,rmc.end_date,rmc.commission_date,rmc.points,rmc.income,CONCAT(rm.firstname,' ',rm.lastname) AS name,rmc.tomato,rmc.other FROM rewards_member_commissions AS rmc INNER JOIN rewards_members AS rm ON rmc.member_id = rm.id LIMIT $offset,$limit");
            if (count($members) > 0) {
                foreach ($members as $member) {
                    $commissions[] = array(
                        "member_name" => $member["name"],
                        "start_date" => $member["start_date"],
                        "end_date" => $member["end_date"],
                        "tomato" => number_format($member["tomato"],2),
                        "other" => number_format($member["other"],2),
                        "points" => number_format($member["points"],2),
                        "income" => number_format($member["income"],2)
                    );
                    $item_count++;
                }    
            }
            
            while ($item_count < $limit) {
                    $commissions[] = array(
                        "member_name" => "",
                        "points" => "",
                        "income" => ""
                    );
                $item_count++;
            }
            
            $info["commissions"] = $commissions;
                        
            GUI::render("admin/commissions.tpl.php",$info);            
        } else {
            parent::redirectTo(ROOT . "logout");
        }
	}
 }
?>