<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 require_once 'core/Pagination.php';
 
 class Search_User extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_ADMIN)) {
            $info = array(
                "pageTitle" => "Manage Users",
                "root" => ROOT
            );
            
            $num_users = 0;
            $num_pages = 1;
            
            $info["num_pages"] = $num_pages;
            $info["current_page"] = 1;
                        
            $users = array();
            $itemCount = 0;
            
            while ($itemCount < 10) {
                $users[] = array(
                    "name" => "",
                    "user" => "",
                    "role" => "",
                    "creator" => "",
                    "date" => "",
                    "status" => ""
                );
                $itemCount++;                
            }
                        
            $info["users"] = $users;
        
            GUI::render("admin/manage.tpl.php",$info);
        } else {
            parent::redirectTo(ROOT . "logout");
        }
	}
    
    private function getUser($user) {
        $result = Db::query(Table::USERS,array("user"),array("id" => $user),"0,1");
        if (count($result) > 0) {
            return $result[0]["user"];
        } else {
            return "Unknown";
        }
    }
 }
?>