<?php
 require_once 'core/Model.php';
 
 class SignUp extends Model {
    public function __construct() {
        parent::__construct($this);
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);
	}
	
	public function render() {
        if (parent::isPostRequest()) {
            self::processRegistration();
        } else {
            self::displaySignupForm();
        }
    }
     
    private function validate($name,$mix,$max) {
        $val = parent::getPost($name);
        if ($val != null || $val != "") {
            $len = strlen($val);
            if ($len > $max) {
            } else if ($len < $min) {
            }
        }
        return $val;
    }

    private function verifyActivationcode($activationcode){
        $query = "SELECT * FROM prime_rewards_db.rewards_registration_code WHERE activation_code='$activationcode' AND activated=0";
        $result = $result = Db::execute($query);
        if(count($result)>0){
           return true;
        }else {
            return false;
        }


    }
     
    public function processRegistration() {
        // P1 = SP 0.2
        // starter kit P1,700
        
        $form_state = true;

        $activation_code = self::validate("activation_code",0,0);
        $firstname = self::validate("firstname",0,0);
        $lastname = self::validate("lastname",0,0);
        $mi = self::validate("mi",0,0);
        $street = self::validate("street",0,0);
        $city = self::validate("city",0,0);
        $state = self::validate("state",0,0);
        $country = self::validate("country",0,0);
        $phone = self::validate("phone",0,0);
        $mobile = self::validate("mobile",0,0);      
        $email = self::validate("email",0,0);
        $gender = self::validate("gender",0,0);
        $dob = self::validate("birthdate",0,0);
        $tin = self::validate("tin",0,0);
        $passport = self::validate("passport",0,0);
        $license = self::validate("drivers_license",0,0);
        $status = self::validate("marital_status",0,0);
        $spouse_name = self::validate("spouse_name",0,0);
        $spouse_contact = self::validate("spouse_contact_no",0,0);
        
        $beneficiary1_name = self::validate("beneficiary1_name",0,0);
        $beneficiary1_relationship = self::validate("beneficiary1_relationship",0,0);
        $beneficiary1_contact_no = self::validate("beneficiary1_contact_no",0,0);
        
        $beneficiary2_name = self::validate("beneficiary2_name",0,0);
        $beneficiary2_relationship = self::validate("beneficiary2_relationship",0,0);
        $beneficiary2_contact_no = self::validate("beneficiary2_contact_no",0,0);
        
        $beneficiary3_name = self::validate("beneficiary3_name",0,0);
        $beneficiary3_relationship = self::validate("beneficiary3_relationship",0,0);
        $beneficiary3_contact_no = self::validate("beneficiary3_contact_no",0,0);
        
        $office_street = self::validate("office_street",0,0);
        $office_city = self::validate("office_city",0,0);
        $office_state = self::validate("office_state",0,0);
        $office_country = self::validate("office_country",0,0);        

        $shipping_street = self::validate("shipping_street",0,0);
        $shipping_city = self::validate("shipping_city",0,0);
        $shipping_state = self::validate("shipping_state",0,0);
        $shipping_country = self::validate("shipping_country",0,0);
        $shipping_zipcode = self::validate("shipping_zipcode",0,0);
        
        $facebook_id = self::validate("facebook_id",0,0);
        $skype_id = self::validate("skype_id",0,0);
        $twitter_id = self::validate("twitter_id",0,0);
        
        $member_of_other_mlm = self::validate("member_of_other_mlm",0,0);
        
        $bank_name = self::validate("bank_name",0,0);
        $bank_account_name = self::validate("bank_account_name",0,0);
        $bank_account_no = self::validate("bank_account_no",0,0);
        $bank_branch = self::validate("bank_branch",0,0);
        
        $sponsor_id = self::validate("sponsor",0,0);
        
        $quantity = self::validate("qty",0,0);
        $product_id = self::validate("product_id",0,0);
        
        $birth_month = intval(parent::getPost("birth_month"));
        $birth_month = $birth_month > 12 ? 12 : $birth_month;
        $birth_month = $birth_month < 1 ? 1 : $birth_month;
        $birth_month = $birth_month < 10 ? "0" . $birth_month : $birth_month;
        
        $birth_day = intval(parent::getPost("birth_day"));
        $birth_day = $birth_day > 31 ? 31 : $birth_day;
        $birth_day = $birth_day < 1 ? 1 : $birth_day;
        $birth_day = $birth_day < 10 ? "0" . $birth_day : $birth_day;
        
        $birth_year = intval(parent::getPost("birth_year"));
        $current_year = date("Y");
        $minimum_age = $current_year - 10;
        $maximum_age = $current_year - 100;
        $birth_year = $birth_year > $minimum_age ? $minimum_age : $birth_year;
        $birth_year = $birth_year < $maximum_age ? $maximum_age : $birth_year;
        
        $dob = $birth_month . "/" . $birth_day . "/" . $birth_year;
        
        if (self::checkIfAccountExists($email)) {
            $form_state = false;
        }
        if(self::verifyActivationcode($activation_code)==false){
            $form_state = false;
            $invalid_activation=true;
            
        }


        if ($form_state) {
            $member_id = Db::insert(Table::MEMBERS,array(
                                            "firstname" => Db::quote($firstname),
                                            "lastname" => Db::quote($lastname),
                                            "mi" => Db::quote($mi),
                                            "street" => Db::quote($street),
                                            "city" => Db::quote($city),
                                            "state" => Db::quote($state),
                                            "country" => Db::quote($country),                                                    
                                            "phone_no" => Db::quote($phone),
                                            "mobile_no" => Db::quote($mobile),
                                            "email" => Db::quote($email),
                                            "gender" => Db::quote($gender),
                                            "dateofbirth" => "STR_TO_DATE(".Db::quote($dob).",'%m/%d/%Y')",
                                            "tin" => Db::quote($tin),
                                            "passport" => Db::quote($passport),
                                            "drivers_license" => Db::quote($license),
                                            "marital_status" => Db::quote($status),
                                            "spouse_name" => Db::quote($spouse_name),
                                            "spouse_contact_no" => Db::quote($spouse_contact),            
                                            "sponsor" => Db::quote($sponsor_id),
                                            "beneficiary1_name" => Db::quote($beneficiary1_name),
                                            "beneficiary1_relationship" => Db::quote($beneficiary1_relationship),
                                            "beneficiary1_contact" => Db::quote($beneficiary1_contact_no),
                                            "beneficiary2_name" => Db::quote($beneficiary2_name),
                                            "beneficiary2_relationship" => Db::quote($beneficiary2_relationship),
                                            "beneficiary2_contact" => Db::quote($beneficiary2_contact_no),
                                            "beneficiary3_name" => Db::quote($beneficiary2_name),
                                            "beneficiary3_relationship" => Db::quote($beneficiary3_relationship),
                                            "beneficiary3_contact" => Db::quote($beneficiary3_contact_no),
                                            "office_street" => Db::quote($office_street),
                                            "office_city" => Db::quote($office_city),
                                            "office_state" => Db::quote($office_state),
                                            "office_country" => Db::quote($office_country),
                                            "shipping_street" => Db::quote($shipping_street),
                                            "shipping_city" => Db::quote($shipping_city),
                                            "shipping_state" => Db::quote($shipping_state),
                                            "shipping_country" => Db::quote($shipping_country),
                                            "shipping_zipcode" => Db::quote($shipping_zipcode),
                                            "facebook_id" => Db::quote($facebook_id),
                                            "skype_id" => Db::quote($skype_id),
                                            "twitter_id" => Db::quote($twitter_id),
                                            "member_of_other_mlm" => Db::quote($member_of_other_mlm),
                                            "bank_name" => Db::quote($bank_name),
                                            "bank_account_name" => Db::quote($bank_account_name),
                                            "bank_account_no" => Db::quote($bank_account_no),
                                            "bank_branch" => Db::quote($bank_branch),
                                            "agree_to_terms" => 1,
                                            "creation_date" => "NOW()",
                                            "image_directory" => "'images/userimage/0.jpg'"
                                           ));
            
            $code = self::generateCode(str_pad($member_id,11,"0",STR_PAD_LEFT),$email);
            $pass = self::generatePass();
    
            $result = self::getProductInfo($product_id);
            //UPDATE ACTIVATION ACCOUNT

            //choi add
            Db::insert(Table::SHOP_CUSTOMERS,array(
                "first_name" => Db::quote($firstname),
                "last_name" =>  Db::quote($lastname),
                "email" =>  Db::quote($email)   
            ));

            if (count($result) > 0) {
                $result = $result[0];
                $in_stock = intval($result["in_stock"]);
                $id = $result["id"];
                $discount = floatval($result["discount"]);
                $total_items = 1;
                $price = floatval($result["price"]);
                $total_price = $price * $quantity;
                if ($in_stock > 0) {    
                    $orderid = Db::insert(Table::ORDERS,array("discount" => $discount,
                                                   "member_id" => $member_id,
                                                   "purchased_on" => "NOW()",
                                                   "status" => "'1'",
                                                   "tax" => "'0'",
                                                   "shipping_price" => "'0'",
                                                   "total_items" => $total_items,
                                                   "total_price" => $total_price));
                    Db::insert(Table::ORDER_ITEMS,array("order_id" => $orderid,"product_id" => $product_id, "price" => $price,"discount" => $discount,"qty" => $quantity));
                } else {    
                    // out of stock! notify us.
                }
            }

            Db::insert(Table::USERS,array("user" => Db::quote(str_pad($member_id,11,"0",STR_PAD_LEFT)),
                                          "email"=> Db::quote($email),
                                          "pass" => Db::quote(User::encodePass($email,$pass)),
                                          "role" => User::USER_ROLE_MEMBER,
                                          "fullname" => Db::quote($firstname . " " . $lastname),
                                          "status" => "1",
                                          "creation_date" => "NOW()"));
            // UPDATE rewards_registration_code

            Db::execute("UPDATE rewards_registration_code 
            SET `activated_by_user`='$member_id', `activated`=1, `date_activated`=NOW() 
            WHERE activation_code='$activation_code'");

            
            Db::insert(Table::ACTIVATION_CODES,array("member_id" => $member_id,
                                                     "activation_code" => Db::quote($code),
                                                     "date_generated" => "NOW()",
                                                     "activated" => "0"));
            
            Db::insert(Table::SHOPS,array("shop_url" => Db::quote(str_replace(" ","",strtolower($firstname.".".$lastname))),
                                          "shop_owner" => $member_id,
                                          "creation_date" => "NOW()"));
            
            $result = Db::query(Table::MEMBERS,array("sponsor"),array("id" => $member_id),"0,1");
            
            if (count($result) > 0) {
                $result = $result[0];
                $sponsor_id = $result["sponsor"];
    
                Db::insert(Table::DOWNLINES,array("parent" => $sponsor_id,
                                                  "child" => $member_id));
            }
    
            self::submitEmail($email,$pass,$member_id,$firstname . " " . $lastname,$code);
         
            //parent::redirectTo(ROOT . "verify-account/member_id/$member_id");
        }else if($invalid_activation==true){
            //redirec to invalid activation
            parent::redirectTo(ROOT . "error/code/102");
        }else {
            parent::redirectTo(ROOT . "error/code/101");
        }
    }
    
    public function submitEmail($email,$pass,$member_id,$fullname,$code) {
        $to      = $email;
        $subject = "Congratulations for joining SPHERE REWARDS!";
        $message = "Hi " . $fullname . "\r\n";
        $message.= "Thanks for signing up! Please use the following email & password to log in:\r\n";
        $message.= "\r\n";
        $message.= "Email: $email\n";
        $message.= "Your temporary password is: $pass\r\n";
        $message.= "Your member id is: " . str_pad($member_id,11,"0",STR_PAD_LEFT) . "\r\n";
        $message.= "\r\n";
        $message.= "Activate your account by clicking the link below:\r\n";
        $message.= ROOT . "verify/ac/" . $code ."\r\n";
        $headers = 'From: Sphere Rewards <webmaster@rewards.sphere.ph>' . "\r\n" .
                   'Reply-To: webmail@rewards.sphere.ph' . "\r\n";
        mail($to,$subject,$message,$headers);
    }
     
    public function getProductInfo($product_id) {
        $result = Db::query(Table::PRODUCTS,array("id","product_type","in_stock","name","price","discount"),
                            array("id" => $product_id),"0,1");
        return $result;
    } 
     
    public function displaySignupForm() {
        $result = Db::query(Table::PRODUCTS,array("id","name","price","description","photo","in_stock"),array("product_type" => "1"),"0,10");
        $products = array();
        if (count($result) > 0) {
            foreach ($result as $product) {
                $qty = array();
                $n = 0;
                while ($n < intval($product["in_stock"])) {
                    $qty[] = $n;
                    $n++;
                }
                $products[] = array(
                    "id" => $product["id"],
                    "photo" => $product["photo"],
                    "name" => $product["name"],
                    "price" => $product["price"],
                    "description" => $product["description"],
                    "qty" => $qty
                );
            }
        }
        
        $months = self::getMonths();
        $days = self::getDays();
        $years = self::getYears();
        $rsUser = Db::execute("SELECT id, CONCAT(lastname, ', ', firstname, ' ',mi,'.') fullname FROM rewards_members");
        
        GUI::render("signup.tpl.php",array('pageTitle' => 'Rewards',
                                           'root' => ROOT,
                                           'products' => $products,
                                           'months' => $months,
                                           'days' => $days,
                                           'years' => $years,
                                           'idList' => $rsUser
                                           ));    
    }
     
    public function generateCode($member_id,$email) {
        $table = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size = strlen($table);
        $code = "";
        $index = 0;
        while ($index < 10) {
            $n = rand(0,$size-1);
            $code.=$table{$n};
            $index++;
        }
        $code = md5(uniqid($member_id.$email, true));
        return $code;
    }
     
    public function generatePass() {
        $table = "abcdefghijklmnopqrstuvwxyz$@!ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size = strlen($table);
        $pass = "";
        $index = 0;
        while ($index < 10) {
            $n = rand(0,$size-1);
            $pass.=$table{$n};
            $index++;
        }
        return $pass;
    }
     
    public function checkIfAccountExists($email) {
        $found = false;
        $result = Db::query(Table::MEMBERS,array("id"),array("email" => Db::quote($email)),null,null);
        if (count($result) > 0) {
            $found = true;
            $result = null;
        }
        return $found;
    }
     
    public function getMonths() {
        $months = array(
            0 => array("id" => 1, "name" => "Jan"),
            1 => array("id" => 2, "name" => "Feb"),
            2 => array("id" => 3, "name" => "Mar"),
            3 => array("id" => 4, "name" => "Apr"),
            4 => array("id" => 5, "name" => "May"),
            5 => array("id" => 6, "name" => "Jun"),
            6 => array("id" => 7, "name" => "Jul"),
            7 => array("id" => 8, "name" => "Aug"),
            8 => array("id" => 9, "name" => "Sep"),
            9 => array("id" => 10, "name" => "Oct"),
            10 => array("id" => 11, "name" => "Nov"),
            11 => array("id" => 12, "name" => "Dec")
        );
        
        return $months;
    }
    
    public function getYears() {
        $years = array();
        
        $current_year = date("Y");
        $minimum_age = $current_year - 10;
        $maximum_age = $current_year - 100;
        $year = $minimum_age;
        
        while ($year > $maximum_age) {
            $years[] = $year;
            $year--;
        }
        return $years;
    }
     
    public function getDays() {
         $days = array();
         $day = 1;
         while ($day <= 31) {
             $days[] = $day;
             $day++;
         }
         return $days;
     }
     
    public function displayOrderInvoice($order_id) {
        $pdf = new InvoicePDF();
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',12);
        
        $result = Db::execute("SELECT ro.`id`, CONCAT(rm.`firstname`, ' ', rm.`lastname`) as `customer_name`, DATE_FORMAT(ro.`purchased_on`,'%W, %M %e, %Y @ %h:%i %p') as date_purchased, ro.`discount`, ro.`total_items`, ro.`tax`, ro.`shipping_price`, ro.`total_price` FROM rewards_orders as ro
INNER JOIN rewards_members as rm ON ro.`member_id` = rm.`id`
INNER JOIN rewards_order_status as rs ON ro.`status` = rs.`id`
WHERE ro.`id` = '$order_id'");    
        $order_no = 0;
        $customer_name = "No Name";
        $purchase_date = "";
        $discount = 0;
        $total = 0;
        $tax = 0;
        $shipping_price = 0;
        
        if (count($result) > 0) {
            $result = $result[0];
            $order_no = str_pad($result["id"],11,"0",STR_PAD_LEFT);
            $customer_name = $result["customer_name"];
            $purchase_date = $result["date_purchased"];
            $discount = $result["discount"];
            $total = $result["total_price"];
            $tax = $result["tax"];
            $shipping_price = $result["shipping_price"];
        }

        $pdf->Cell(38,7,"Order No.: ",0,0);
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(24,7,$order_no,0,0);
        $pdf->Ln();
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(38,7,"Customer Name: ",0,0);
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(200,7,$customer_name,0,0);
        $pdf->Ln();
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(38,7,"Purchase Date: ",0,0);
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(120,7,$purchase_date,0,0);
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Cell(24,7,"Quantity",1,0,'C');
        $pdf->Cell(144,7,"Description",1,0,'C');
        $pdf->Cell(20,7,"Price",1,0,'C');
        $pdf->Ln();
        $pdf->SetFont('Arial','',10);
        
        $result = Db::execute("SELECT ri.`price`, ri.`discount`, ri.`qty`,rp.`name` FROM `rewards_order_items` as ri 
INNER JOIN `rewards_products` AS rp ON ri.`product_id` = rp.`id`
WHERE ri.`order_id` = '$order_id'");
        
        if (count($result) > 0) {
            foreach ($result as $item) {
                $pdf->Cell(24,7,$item["qty"],0,0,'L');
                $pdf->Cell(144,7,$item["name"],0,0,'L');
                $pdf->Cell(20,7,"P " . $item["price"],0,0,'R');
                $pdf->Ln();
            }
        }
        
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Cell(24,7,"Discount",0,0,'L');
        $pdf->Cell(144,7,"",0,0,'L');
        $pdf->Cell(20,7,"$discount",0,0,'R');
        $pdf->Ln(); 
        $pdf->Cell(24,7,"Shipping Fee",0,0,'L');
        $pdf->Cell(144,7,"",0,0,'L');
        $pdf->Cell(20,7,"$shipping_price",0,0,'R');
        $pdf->Ln();
        $pdf->Cell(24,7,"Tax",0,0,'L');
        $pdf->Cell(144,7,"",0,0,'L');
        $pdf->Cell(20,7,"$tax",0,0,'R');
        $pdf->Ln();                    
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Arial','B',18);
        $pdf->Cell(24+140,7,"Total",0,0,'L');
        $pdf->SetFont('Arial','',18);
        $pdf->Cell(24,7,"P $total",0,0,'R');    

        $pdf->Output();        
    }     
 }

class InvoicePDF extends FPDF {
    function Header() {
        $this->Image('images/sphere/mlm-logo.png',10,6,30);
        $this->SetFont('Arial','B',10);
        $this->Cell(80);
        $this->Cell(30,10,'Sphere Rewards Invoice',0,1,'C');
        $this->Ln(30);
    }
    
    function Footer() {
        $this->SetY(-15);
        $this->SetFont('Arial','I',8);
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
}
?>