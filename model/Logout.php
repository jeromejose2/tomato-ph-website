<?php
 require_once 'core/Model.php';
 
 class Logout extends Model {
    public function __construct() {
        parent::__construct($this);
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);
	}
	
	public function render() {
        Session::start();        
        Session::destroy();
        
        parent::redirectTo(ROOT . "home");
	}	
 }
?>