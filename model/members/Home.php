<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 
 class Home extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        $user_id = Session::get("user_id2");
        $u_id = Session::get("user_id");
		
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_MEMBER)) {
            
        
            $info = array(
                "pageTitle" => "Home",
                "last_login" => ""
            );
            
//             $result = Db::execute("SELECT ru.`id`, ru.`user`, ru.`fullname`,rm.`id`, rm.`email`, rm.`phone_no`, rm.`mobile_no`,rm.`street`, rm.`city`, rm.`state`, rm.`country`, DATE_FORMAT(rm.`dateofbirth`, '%M %e, %Y') AS `dateofbirth` FROM `rewards_users` AS ru 
// INNER JOIN `rewards_members` AS rm ON ru.`user` = rm.`id` 
// WHERE ru.`id` = '$user_id' LIMIT 0,1");

            $result = Db::db_execute(USE_SPHERE_DB, "SELECT * FROM shop_customers a WHERE a.id = $user_id");
            $pad = str_pad($u_id,11,"0",STR_PAD_LEFT);;
            $image = Db::execute("SELECT image_directory FROM rewards_members WHERE id = $pad");

            if (count($result) > 0) {
                $result = $result[0];
                $login_result = Db::execute("SELECT ID, DATE_FORMAT(login_date,'%W, %M %e, %Y @ %h:%i %p') AS lastlogin FROM rewards_login_history WHERE user_id = $user_id ORDER BY ID DESC limit 0,1 ");
                $result2 = Db::execute("SELECT * FROM rewards_users WHERE id = $u_id");
                $info["last_login"] = date("F j, Y, g:i a");
                if (count($login_result) > 1) {
                    $info["last_login"] = $login_result[1]["lastlogin"];
                }
                $info["member_name"] = $result["last_name"] . ', '. $result["first_name"];
                
                $info["member_id"] = str_pad($result["id"],11,"0",STR_PAD_LEFT);
                //$info["birthday"] = $result["dateofbirth"];
                $info["landline_no"] = $result["phone"];
                // $info["mobile_no"] = $result["mobile_no"];
                $info["address"] = $result["shipping_street_addr"] . ",\n" . $result["shipping_city"]; //. ",\n" . $result["country"];
                $info["email"] = $result["email"];
                $info["image"] = $image[0]["image_directory"];

            }
            
            $commission = Db::execute("SELECT SUM(rc.`points`) AS `points` FROM `rewards_member_commissions` AS rc INNER JOIN rewards_members AS rm ON rc.member_id = rm.id INNER JOIN rewards_users AS ru ON rm.id = ru.user WHERE ru.`id` = '$user_id'");
            $sp = "0";
            if (count($commission) > 0) {
                $sp = $commission[0]["points"] == "" ? "0" : $commission[0]["points"];
            }
            
            $info["announcement"] =  Db::execute("SELECT * FROM  `rewards_company_announcement` LIMIT 5");
            $info["points"] = $sp;
            
            GUI::render("member/home.tpl.php",$info);
        } else {
           parent::redirectTo(ROOT . "logout");
		     
        }
	}
 }
?>