<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 require_once 'core/Pagination.php';
 
 class Account_Settings extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_MEMBER)) {
            self::displaySettings();
        } else {
            parent::redirectTo(ROOT . "logout");
        }
	}
          
    public function displaySettings() {
        $info = array(
            "pageTitle" => "Account Settings",
            "root" => ROOT,
        );
        
        GUI::render("member/account_settings.tpl.php",$info);
    }
 }
?>