<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 
 class ChangePassword extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_MEMBER)) {
            if (parent::isPostRequest()) {
                $result = self::updatePass($user_id);
                if (!$result) {
                    self::displayChangePassResult("Success","Password has been changed successfully");                            
                } else if ($result == 1) {
                    self::displayChangePassResult("Error","Password does not match the confirm password");                        
                } else if ($result == 2) {
                    self::displayChangePassResult("Error","Old password does not match current password");
                }
            } else {
                self::displayChangePass();
            }
        } else {
            parent::redirectTo(ROOT . "logout");
        }
	}
    
    public function displayChangePass() {
        $info = array("pageTitle" => "Change Password");
        GUI::render("member/change_password.tpl.php",$info);
    }
     
    public function displayChangePassResult($title,$message) {
        $info = array("pageTitle" => $title,
                      "message" => $message);
        GUI::render("member/change_password_result.tpl.php",$info);       
    } 
    
    public function updatePass($user_id) {
        $oldpass = parent::getPost("oldpass");
        $newpass = parent::getPost("newpass");
        $confirmpass = parent::getPost("confirmpass");
        
        $pass = "";
        $user = "";
        
        $state = 0;
        
        $result = Db::execute("SELECT ru.`user`, ru.`pass`,ru.`email` FROM `rewards_users` AS ru WHERE ru.`id` = '$user_id'");
        if (count($result) > 0) {
            $result = $result[0];
            $pass = $result["pass"];
            $user = $result["email"];
        }
        
        if ($newpass != $confirmpass) {
            $state = 1;
        } else {
            $oldpass = User::encodePass($user,$oldpass);
            if ($pass != $oldpass) {
                $state = 2;
            } else {
                $newpass = User::encodePass($user,$newpass);
                $user_id = Session::get("user_id");
                Db::execute("UPDATE rewards_users SET pass=" . Db::quote($newpass) . " WHERE id='" . $user_id . "'");
            }
        }
        
        return $state;
    } 

 }
?>