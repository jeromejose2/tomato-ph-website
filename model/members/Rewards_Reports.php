<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 
 class Rewards_Reports extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        Session::start();
        
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_MEMBER)) {
            $extras = parent::getExtras();
            self::displayReport($user_id);
        } else {
            parent::redirectTo(ROOT . "logout");
        }
	}
     
    public function displayReport($user_id) {
            $info = array("pageTitle" => "Rewards Reports"); 
                        
            GUI::render("member/rewards_reports.tpl.php",$info);    
    } 
 }
?>