<?php
 require_once 'core/Model.php';
 
 class Complete extends Model {
    public function __construct() {
        parent::__construct($this);
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);
	}
	
	public function render() {
		GUI::render("complete.tpl.php",array('pageTitle' => 'Complete',
                                         'root' => ROOT,
                                        ));
        
	}	
 }
?>