<?php
 require_once 'core/Model.php';
 
 class Payment_Method extends Model {
    public function __construct() {
        parent::__construct($this);
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);
	}
	
	public function render() {
        $info = array('pageTitle' => 'Payment Method',
                      'root' => ROOT);
		GUI::render("payment_method.tpl.php",$info);
    }
 }
?>