<?php
 require_once 'core/Model.php';
 
 class Activate extends Model {
    public function __construct() {
        parent::__construct($this);
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);
	}
	
	public function render() {

        Session::start();
        $user_id = Session::get("user_id");
        if (User::isAuthenticated($user_id) && User::hasRole(User::USER_ROLE_MEMBER)) {
            GUI::render("activate.tpl.php",array("pageTitle" => "Activate Accont"));            
        } else {
            parent::redirectTo(ROOT . "home");
        }
	}	
 }
?>