<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 require_once 'core/Pagination.php';

 define ('LEVEL_DEPTH',5);
 define ('TOMATO_BRANDS_ONE_SP',0.02);  // for tomato brands
 define ('OTHER_BRANDS_ONE_SP',0.006); // for other brands

 define ('TOMATO_BRAND',1);
 define ('OTHER_BRAND',2);

 define ('MR',850);

 /*
  For Tomato, Tomato Time and SWAP = 0.2 SP = P1
  Other Brands = 0.06 SP = P1
  
  Product link/page:
   109 = SWAP
   34 = Tomato Time
   32 = Tomato
  */

 define('SWAP',109);
 define('TOMATO',32);
 define('TOMATO_TIME',34);;
 
 class Compute_Commissions extends Model {
    
    public function __construct() {
        parent::__construct($this);     
    }
    
    public function create($extras=null) {
        parent::setExtras($extras);     
    }
    
    public function render() {
        $commission_id = self::computeCommissions();
        self::updateDownlineCommissions($commission_id);
    }
     
    public function getPurchases($email) {
        $orders = Db::db_execute(USE_SPHERE_DB,
        "SELECT soi.price,soi.discount,soi.quantity,pp.label,sp.page_id 
        FROM shop_orders AS so 
        INNER JOIN shop_order_items AS soi ON so.id = soi.shop_order_id 
        INNER JOIN shop_products AS sp ON soi.shop_product_id = sp.id 
        INNER JOIN pages AS pp ON sp.page_id = pp.id 
        INNER JOIN shop_customers AS scu ON so.customer_id = scu.id WHERE scu.email = '$email'");
        $ttl_tomato = 0;
        $ttl_other_brands = 0;
        if (count($orders) > 0) {
            foreach ($orders as $order) {
                $price = $order["price"];
                $quantity = $order["quantity"];
                $total = $order["price"] * $quantity;
                $discount = $order["discount"];
                $page_id = $order["page_id"];
                
                if ($discount == 0) {
                    if ($page_id == $SWAP || $page_id == $TOMATO_TIME || $page_id == $TOMATO) {
                        $ttl_tomato += $total;
                    } else {
                        $ttl_other_brands;
                    }
                }                
            }
        }
        
        return array($ttl_tomato,$ttl_other_brands);
    }
     
    private function computeCommissions() {
        $commission_id = Db::insert("rewards_commissions",
            array(
                "commission_date" => "NOW()",
               "start_date" => "DATE_SUB(NOW(),INTERVAL 14 DAY)",
               //"start_date" => "NOW()",
                "end_date" => "NOW()")
            );
        $members = Db::execute("SELECT id,email,income,points FROM rewards_members");
        if (count($members) > 0) {
            foreach ($members as $member) {
                $email = $member["email"];
                $member_id = $member["id"];
                $member_income = $member["income"];
                $member_points = $member["points"];

                $ttl = 0;
                $ttl_tomato = 0;
                $ttl_other_brands = 0;
                $sp = 0;
                $income = 0;
                $percent = 0;

                $orders = Db::db_execute(USE_SPHERE_DB,
                        "SELECT so.id,soi.shop_product_id,so.order_datetime,sp.name 
                        AS product_name,soi.price,soi.quantity,pp.label,sp.page_id 
                        FROM shop_orders AS so 
                        INNER JOIN shop_order_items AS soi ON so.id = soi.shop_order_id 
                        INNER JOIN shop_products AS sp ON soi.shop_product_id = sp.id 
                        INNER JOIN pages AS pp ON sp.page_id = pp.id 
                        INNER JOIN shop_order_statuses AS sos ON so.status_id = sos.id 
                        INNER JOIN shop_customers AS sc ON so.customer_id = sc.id 
                        WHERE sc.email='$email' AND sos.id = 2 OR sos.id = 3 
                        AND soi.discount = 0 
                        AND so.order_datetime >= DATE_SUB(NOW(),INTERVAL 14 DAY) 
                        AND so.order_datetime <= NOW()");

                if (count($orders) > 0) {
                    foreach ($orders as $order) {
                        $order_id = $order["id"];
                        $product_id = $order["shop_product_id"];
                        $product_name = $order["product_name"];
                        $order_date = $order["order_datetime"];
                        $price = number_format($order["price"],2);
                        $quantity = $order["quantity"];
                        $total = $order["price"] * $quantity;
                        $label = $order["label"];
                        $page_id = $order["page_id"];
                        
                        if ($page_id == SWAP || $page_id == TOMATO_TIME || $page_id == TOMATO) {
                            $ttl_tomato += $total;
                        } else {
                            $ttl_other_brands += $total;
                        }     

                        Db::insert("rewards_member_orders",array("shop_order_id" => $order["id"],
                                                                 "product_id" => $order["shop_product_id"],
                                                                 "product_name" => Db::quote($order["product_name"]),
                                                                 "order_datetime" => Db::quote($order["order_datetime"]),
                                                                 "price" => $order["price"],
                                                                 "quantity" => $order["quantity"],
                                                                 "label" => Db::quote($order["label"]),
                                                                 "page_id" => $order["page_id"],
                                                                 "member_id" => $member_id,
                                                                 "created_at" => "NOW()"));
                    }
                }
                
                $ttl = $ttl_tomato + $ttl_other_brands;
                if ($ttl >= MR) {
                    $percent = 0.05;
                    $sp = ($ttl_tomato * $percent) * TOMATO_BRANDS_ONE_SP;
                    $sp += ($ttl_other_brands * $percent) * OTHER_BRANDS_ONE_SP;
                    $income = ($ttl_tomato + $ttl_other_brands) * $percent;
                } else if ($ttl >= 136 && $ttl <= 849) {
                    $percent = 0.04;
                    $sp = ($ttl_tomato * $percent) * TOMATO_BRANDS_ONE_SP;
                    $sp += ($ttl_other_brands * $percent) * OTHER_BRANDS_ONE_SP; 
                    $income = ($ttl_tomato + $ttl_other_brands) * $percent;
                } else if ($ttl >= 102 && $ttl <= 135) {
                    $percent = 0.03;
                    $sp = ($ttl_tomato * $percent) * TOMATO_BRANDS_ONE_SP;
                    $sp += ($ttl_other_brands * $percent) * OTHER_BRANDS_ONE_SP;   
                    $income = ($ttl_tomato + $ttl_other_brands) * $percent;
                } else if ($ttl >= 68 && $ttl <= 101) {
                    $percent = 0.02;
                    $sp = ($ttl_tomato * $percent) * TOMATO_BRANDS_ONE_SP;
                    $sp += ($ttl_other_brands * $percent) * OTHER_BRANDS_ONE_SP;
                    $income = ($ttl_tomato + $ttl_other_brands) * $percent;
                } else if ($ttl >= 34 && $ttl <= 67) {
                    $percent = 0.01;
                    $sp = ($ttl_tomato * $percent) * TOMATO_BRANDS_ONE_SP;
                    $sp += ($ttl_other_brands * $percent) * OTHER_BRANDS_ONE_SP;   
                    $income = ($ttl_tomato + $ttl_other_brands) * $percent;
                }                                    
                Db::execute("INSERT INTO rewards_member_commissions 
                            (
                            start_date,end_date,created_at,commission_date,points,income,
                            member_id,tomato,other,commission_id
                            ) 
                            VALUES(DATE_SUB(NOW(),INTERVAL 14 DAY),NOW(),NOW(),NOW(),
                            $sp,$income,$member_id,$ttl_tomato,$ttl_other_brands,$commission_id)");
                if ($income > 0) {
                    $member_income += $income;
                    $member_points += $sp;
                } else {
                    if ($member_income >= MR) {
                        $member_income -= MR;
                    } else {
                        if ($member_income > 0) {
                            $member_income = MR - $member_income;
                        } else {
                            $member_income = 0;
                        }
                    }
                }
                 echo"INCOME".$income."POINTS".$sp."<br/>";
                echo "WHOLE INCOME" .$member_income."POINTS".$member_points."<br/>";   
                      
                Db::execute("UPDATE rewards_members 
                            SET income = $member_income, points = $member_points 
                            WHERE id=$member_id");
            }
        } 
        return $commission_id;
    }
     
    private function updateDownlineCommissions($commission_id) {
        $commissions = Db::execute("SELECT rmc.* 
            FROM rewards_member_commissions AS rmc 
            INNER JOIN rewards_members AS rm ON rmc.member_id = rm.id 
            WHERE commission_id = '$commission_id'");
        if (count($commissions) > 0) {
            foreach ($commissions as $commission) {
                $ttl = $commission["tomato"] + $commission["other"];
                if ($ttl > 0) {
                    $level = 0;
                    $sponsor_id = $commission["member_id"];
                    while ($level < LEVEL_DEPTH) {
                        $dnl = Db::execute("SELECT * 
                            FROM rewards_downlines 
                            WHERE parent='$sponsor_id'");
                        if (count($dnl) > 0) {
                            $sponsor_id = $dnl["child"];
                            print $sponsor_id . "<br>";
                            $level++;
                        } else {
                            break;
                        }
                    }
                }
            }
        }
    } 
     
    public function convToSP($value,$brand) {
        if ($brand == TOMATO_BRANDS) {
            return $value * TOMATO_BRANDS_ONE_SP;
        } else {
            return $value * OTHER_BRANDS_ONE_SP;
        }
    } 
 }
?>