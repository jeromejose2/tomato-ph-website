<?php
 require_once 'core/Model.php';
 require_once 'core/Gui.php';
 require_once 'core/Pagination.php';

 define ('LEVEL_DEPTH',5);
 define ('TOMATO_BRANDS_ONE_SP',0.2);  // for tomato brands
 define ('OTHER_BRANDS_ONE_SP',0.06); // for other brands

 define ('TOMATO_BRAND',1);
 define ('OTHER_BRAND',2);

 define ('MR',850);

 /*
  For Tomato, Tomato Time and SWAP = 0.2 SP = P1
  Other Brands = 0.06 SP = P1
  
  Product link/page:
   109 = SWAP
   34 = Tomato Time
   32 = Tomato
  */
 
 class Compute_Commissions extends Model {
	
    public function __construct() {
        parent::__construct($this);		
	}
	
	public function create($extras=null) {
	    parent::setExtras($extras);		
	}
	
	public function render() {
        $commission_id = Db::insert("rewards_commissions",
            array("commission_date" => "NOW()","start_date" => "DATE_SUB(NOW(),INTERVAL 14 DAY)","end_date" => "NOW()"));
        $members = Db::execute("SELECT rm.id,rm.income,rm.points,rm.email,CONCAT(rm.firstname,' ',rm.lastname) AS name FROM rewards_members AS rm");
        if (count($members) > 0) {
            
            $SWAP = 109;
            $TOMATO = 32;
            $TOMATO_TIME = 34;
                        
            foreach ($members as $member) {
                $sep = "&nbsp;&nbsp;&nbsp;&nbsp;";

                $member_id = $member["id"];
                $name = $member["name"];
                $member_income = $member["income"];
                $member_points = $member["points"];
                $email = $member['email'];
                
                print "Computing $name's commission ($email)<br>";

                $result = Db::db_execute(USE_SPHERE_DB,"SELECT scu.id,scu.email FROM shop_customers AS scu WHERE scu.email = " . Db::quote($member["email"]));
                
                $ttl = 0;
                $ttl_tomato = 0;
                $ttl_other_brands = 0;
                $sp = 0;
                $income = 0;
                $percent = 0;
            
                if (count($result) > 0) {
                    $result = $result[0];
                    $id = $result["id"];
                    
                    $orders = Db::db_execute(USE_SPHERE_DB,"SELECT soi.shop_product_id,so.order_datetime,sp.name AS product_name,soi.price,soi.discount,soi.quantity,pp.label,sp.page_id FROM shop_orders AS so INNER JOIN shop_order_items AS soi ON so.id = soi.shop_order_id INNER JOIN shop_products AS sp ON soi.shop_product_id = sp.id INNER JOIN pages AS pp ON sp.page_id = pp.id WHERE so.customer_id = '$id'");
                    foreach ($orders as $order) {
                        $product_id = $order["shop_product_id"];
                        $product_name = $order["product_name"];
                        $order_date = $order["order_datetime"];
                        $price = number_format($order["price"],2);
                        $quantity = $order["quantity"];
                        $total = $order["price"] * $quantity;
                        $discount = number_format($order["discount"],2);
                        $label = $order["label"];
                        $page_id = $order["page_id"];
                        
                        if ($discount == 0) {
                            if ($page_id == $SWAP || $page_id == $TOMATO_TIME || $page_id == $TOMATO) {
                                $ttl_tomato += $total;
                            } else {
                                $ttl_other_brands;
                            }
                        }
                    }
                    $ttl = $ttl_tomato + $ttl_other_brands;
                    if ($ttl >= 850) {
                        $percent = 0.05;
                        $sp = ($ttl_tomato * $percent) * TOMATO_BRANDS_ONE_SP;
                        $sp += ($ttl_other_brands * $percent) * OTHER_BRANDS_ONE_SP;
                        $income = ($ttl_tomato + $ttl_other_brands) * $percent;
                    } else if ($ttl >= 136 && $ttl <= 849) {
                        $percent = 0.04;
                        $sp = ($ttl_tomato * $percent) * TOMATO_BRANDS_ONE_SP;
                        $sp += ($ttl_other_brands * $percent) * OTHER_BRANDS_ONE_SP; 
                        $income = ($ttl_tomato + $ttl_other_brands) * $percent;
                    } else if ($ttl >= 102 && $ttl <= 135) {
                        $percent = 0.03;
                        $sp = ($ttl_tomato * $percent) * TOMATO_BRANDS_ONE_SP;
                        $sp += ($ttl_other_brands * $percent) * OTHER_BRANDS_ONE_SP;   
                        $income = ($ttl_tomato + $ttl_other_brands) * $percent;
                    } else if ($ttl >= 68 && $ttl <= 101) {
                        $percent = 0.02;
                        $sp = ($ttl_tomato * $percent) * TOMATO_BRANDS_ONE_SP;
                        $sp += ($ttl_other_brands * $percent) * OTHER_BRANDS_ONE_SP;
                        $income = ($ttl_tomato + $ttl_other_brands) * $percent;
                    } else if ($ttl >= 34 && $ttl <= 67) {
                        $percent = 0.01;
                        $sp = ($ttl_tomato * $percent) * TOMATO_BRANDS_ONE_SP;
                        $sp += ($ttl_other_brands * $percent) * OTHER_BRANDS_ONE_SP;   
                        $income = ($ttl_tomato + $ttl_other_brands) * $percent;
                    }                    
                }
                                
                Db::execute("INSERT INTO rewards_member_commissions (start_date,end_date,created_at,commission_date,points,income,member_id,tomato,other,commission_id) VALUES(DATE_SUB(NOW(),INTERVAL 14 DAY),NOW(),NOW(),NOW(),$sp,$income,$member_id,$ttl_tomato,$ttl_other_brands,$commission_id)");
                if ($income > 0) {
                    $member_income += $income;
                    $member_points += $sp;
                } else {
                    if ($member_income >= MR) {
                        $member_income -= MR;
                    } else {
                        if ($member_income > 0) {
                            $member_income = MR - $member_income;
                        } else {
                            $member_income = 0;
                        }
                    }
                }
                
                print "$sep points: SP ".number_format($sp,2)."<br>";
                print "$sep commission: &#x20b1; ".number_format($income,2)." ((tomato * 0.2) + (other brands * 0.06) * $percent)<br>";
                print "$sep tomato brands: &#x20b1; ".number_format($ttl_tomato,2)."<br>";
                print "$sep other brands: &#x20b1; ".number_format($ttl_other_brands,2)."<br>";

                Db::execute("UPDATE rewards_members SET income = $member_income, points = $member_points WHERE id=$member_id");
                $sep .= $sep;
                print "$sep Computing commissions for downlines<br>";
                
                $sponsor_id = $member_id;
                $level = 1;
                $count = 0;
                $sep .= $sep;
                while ($level <= 5) {
                    $downlines = Db::execute("SELECT rd.child,CONCAT(rm.firstname,' ',rm.lastname) AS name,rm.email FROM rewards_downlines AS rd INNER JOIN rewards_members AS rm ON rd.child = rm.id WHERE parent=$sponsor_id");
                    if (count($downlines) > 0) {
                        $count++;
                        $name = $downlines[0]["name"];
                        $sponsor_id = $downlines[0]["child"];
                        print "$sep LVL $level $name<br>";
                        $ret = self::getPurchases($email);
                        $ttl_tomato = $ret[0];
                        $ttl_other_brands = $ret[1];
                        $ttl = $ttl_tomato + $ttl_other_brands;
                        
                        if ($ttl >= 850) {
                            $percent = 0.03;
                            $sp = ($ttl_tomato * $percent) * TOMATO_BRANDS_ONE_SP;
                            $sp += ($ttl_other_brands * $percent) * OTHER_BRANDS_ONE_SP;
                            $income = ($ttl_tomato + $ttl_other_brands) * $percent;
                            
                            $sep .= $sep;
                            print "$sep points: $sp<br>";
                            print "$sep commision: &#x20b1; ".number_format($income,2) ." ((tomato * 0.2) + (other brands * 0.06) * $percent)<br>";
                            print "$sep tomato brands: &#x20b1; ".number_format($ttl_tomato,2) ."<br>";
                            print "$sep other brands: &#x20b1; ".number_format($ttl_other_brands,2) ."<br>";
                            $level++;
                        } else {
                            print "$sep puchases $ttl &lt; MR<br>";
                        }
                    } else {
                        break;
                    }
                }
                if ($count == 0) {
                    print "$sep No downlines<br>";
                }
                
                print "<br>";
            }
        }
	}
     
    public function getPurchases($email) {
        $orders = Db::db_execute(USE_SPHERE_DB,"SELECT soi.price,soi.discount,soi.quantity,pp.label,sp.page_id FROM shop_orders AS so INNER JOIN shop_order_items AS soi ON so.id = soi.shop_order_id INNER JOIN shop_products AS sp ON soi.shop_product_id = sp.id INNER JOIN pages AS pp ON sp.page_id = pp.id 
        INNER JOIN shop_customers AS scu ON so.customer_id = scu.id WHERE scu.email = '$email'");
        $ttl_tomato = 0;
        $ttl_other_brands = 0;
        if (count($orders) > 0) {
            foreach ($orders as $order) {
                $price = $order["price"];
                $quantity = $order["quantity"];
                $total = $order["price"] * $quantity;
                $discount = $order["discount"];
                $page_id = $order["page_id"];
                
                if ($discount == 0) {
                    if ($page_id == $SWAP || $page_id == $TOMATO_TIME || $page_id == $TOMATO) {
                        $ttl_tomato += $total;
                    } else {
                        $ttl_other_brands;
                    }
                }                
            }
        }
        
        return array($ttl_tomato,$ttl_other_brands);
    }
     
    public function convToSP($value,$brand) {
        if ($brand == TOMATO_BRANDS) {
            return $value * TOMATO_BRANDS_ONE_SP;
        } else {
            return $value * OTHER_BRANDS_ONE_SP;
        }
    } 
 }
?>