<?php

/*
 * Developed by Cris del Rosario
 */

require_once 'core/Loader.php';

Loader::register();
Loader::bootup();
?>