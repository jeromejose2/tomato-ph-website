        <footer>
        	<div id="foot-hold">			
            	<div id="ft_box1" class="ft_box">
                    <h2 class="ft_headings small">JOIN OUR MAILING LIST FOR NEWS &amp; UDPATES</h2>
                    <form id="mailinglist_form" action="" method="">
                        <fieldset>
                        <input id="fld_maillist" class="fl" type="text" />
                        <input id="btn_maillist" class="fl" type="submit" value="GO" />                            
                        </fieldset>
                    </form>
                <div class="clearfix">&nbsp;</div>
                <img src="{{root}}images/sphere/payment.png" alt="" usemap="#Map" border="0" />
                    <map name="Map"> 
                        <area shape="rect" coords="20,19,243,55" href="http://www.sphere.ph/how-to-pay-via-paypal/" target="_blank" />
                        <area shape="rect" coords="20,71,247,126" href="http://www.sphere.ph/how-to-pay-via-online-fundtransfer/" target="_blank" />
                        <area shape="rect" coords="19,142,249,175" href="http://www.sphere.ph/how-to-pay-via-bank-deposit/" target="_blank" />      
                        <area shape="rect" coords="16,192,249,235" href="http://www.sphere.ph/how-to-pay-via-mobile/" target="_blank" />
                        <area shape="rect" coords="19,249,78,288" href="http://www.sphere.ph/how-to-pay-via-remittance" target="_blank" />
                        <area shape="rect" coords="88,252,179,287" href="http://www.sphere.ph/how-to-pay-via-dragonpay/" target="_blank" />
                        <area shape="rect" coords="188,254,251,288" href="http://www.sphere.ph/how-to-pay-via-egc/" target="_blank" />
                    </map>
                </div>
                <div id="ft_box2" class="ft_box">
                    <h2 class="ft_headings borderbottom">HELP</h2>
                    <ul>
                        <li><a href="http://sphere.ph/contact-us">Contact Us</a></li>
                        <li><a href="http://sphere.ph/store-locator">Store Locator</a></li>
                        <li><a href="http://sphere.ph/size-charts">Size Charts</a></li>
                        <li><a href="http://sphere.ph/faq/general">FAQ</a></li>
                        <li><a href="http://sphere.ph/how-to-order">How to order</a></li>
                        <li><a href="http://sphere.ph/payment-options">Payment instructions</a></li>
                        <li><a href="http://sphere.ph/return-policy">Return Policy</a></li>
                        <li><a href="http://sphere.ph/shipping-advisory">Shipping advisory</a></li>
                        <li><a href="http://sphere.ph/nationwide-destination">Nationwide destination</a></li>
                        <li><a href="http://sphere.ph/worldwide-shipping">Worldwide shipping</a></li>
                        <li><a href="http://sphere.ph/terms-and-condition">Terms &amp; Conditions</a></li>
                    </ul>
                </div>
                <div id="ft_box3" class="ft_box">
                    <h2 class="ft_headings borderbottom">BRANDS</h2>
                    <ul>
                        <li><a href="http://sphere.ph/tomato">Tomato</a></li>
                        <li><a href="http://sphere.ph/time">Tomato Time</a></li>
                        <li><a href="http://sphere.ph/swap">Swap</a></li>
                    </ul>
                </div>
                <div id="ft_box4" class="ft_box">
                    <h2 class="ft_headings borderbottom">RECRUITMENT &amp; INCENTIVES</h2>
                    <ul>
                        <li><a href="http://sphere.ph/howtoearn">How to earn</a></li>
                        <li><a href="http://sphere.ph/commissions">Commissions</a></li>
                        <li><a href="http://sphere.ph/incentives">Incentives</a></li>
                        <li><a href="http://sphere.ph/videos">Videos</a></li>
                    </ul>
                    <h2 class="ft_headings sm_margin">CONNECT WITH US</h2>
                    <img style="margin: 0 0 5px 0;" src="{{root}}images/sphere/socialmedia.png" alt="" usemap="#Map9" border="0" />
                    <map name="Map9">
                        <area shape="circle" coords="131,16,13" href="http://pinterest.com/tomatoph/" alt="pinterest" target="_blank" />
                        <area shape="circle" coords="94,16,13" href="http://instagram.com/tomatoph" alt="instagram" target="_blank" />
                        <area shape="circle" coords="54,16,13" href="https://twitter.com/tomatoph" alt="twitter" target="_blank" />
                        <area shape="circle" coords="16,16,13" href="http://www.facebook.com/TomatoBeDelicious" alt="facebook" target="_blank" />
                        <area shape="circle" coords="170,16,14" href="http://www.youtube.com/tomatophl" />
                    </map> 
                    <iframe style="overflow: hidden; width: 200px; height: 80px;" src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2FTomatoBeDelicious&amp;send=false&amp;layout=standard&amp;width=200&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=80" frameborder="0" scrolling="no"></iframe>
                </div>
                <div id="ft_box5" class="ft_box last">
                    <h2 class="ft_headings borderbottom">SPHERE</h2>
                    <ul>
                        <li><a href="http://sphere.ph/about">About</a></li>
                        <li><a href="http://sphere.ph/work-with-us">Work with us</a></li>
                        <li><a href="http://sphere.ph/blog">Blog</a></li>
                    </ul>
                </div>
                <script type="text/javascript">// <![CDATA[
                    $j('.tomato .product_list li').addClass('productItem');
                    $j('.tomato .productItem').appendTo('#mycarousel');
                    $j('.tomato .product_list').remove();
                // ]]></script>
                <div class="clearfix"></div>
            </div>
        </footer>
        <script>
		</script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src='//www.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>