{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "nav.tpl.php" %}
    <div id="cont-wrap">
        <br>
        <br>
        <br>
        <br>
        <div class="common_product_container">
            <div class="shopcart_left_container">
                {% for item in products %}
                <div class="product_item" align="center">
                    <img src="{{root}}{{item.photo}}" style="width: 120px"/>
                    <br>
                    <br>
                    <a href="{{root}}">{{item.name}}</a><br>
                    <b>Price:</b> P {{item.price}}<br>
                    <input type="button" class="button" name="add_to_cart" value="Add To Cart"/>
                </div>
                {% endfor %}
            </div>
            <div class="shopcart_right_container">
                <button type="button" name="view_cart" class="form_button">View Your Cart</button>
            </div>
        </div>
    </div>
{% endblock content %}