{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
{% include "nav.tpl.php" %}
<div id="cont-wrap">
    <br>
    <br>
    <br>
    <br>
    <div class="common_form_container">
        <center><h1 class="page-title">Registration Form</h1></center>
        <br>
        <form id="signup_form" action="{{root}}signup" method="post">
            <fieldset>
                <div class="field_item_div">
                    <div class="normal_label"><label>Personal Information</label></div>
                </div>                        
                <div class="field_item">
                    <label>Name<span>*</span></label><br>
                    <input type="text" name="firstname" class="text" placeholder="First Name" required/>
                    <input type="text" name="lastname" class="text" placeholder="Last Name" required/>
                    <input type="text" name="mi" size="3" class="text" placeholder="M.I." required/>
                </div>
                <div class="field_item">
                    <label>Home Address<span>*</span></label><br>
                    <input type="text" name="street" class="text" placeholder="Street" size="50" required/>
                    <input type="text" name="city" class="text" placeholder="City" required/><br>
                    <input type="text" name="state" class="text" placeholder="State" required/><br>
                    <select name="country">
                        <option value="PH">Philippines</option>
                    </select>
                </div>
                <div class="field_item">
                    <label>Phone No<span>*</span></label><br>
                    <input type="text" class="text" name="phone" required/>
                </div>
                <div class="field_item">
                    <label>Mobile No<span>*</span></label><br>
                    <input type="text" class="text" name="mobile" required/>
                </div>
                <div class="field_item">
                    <label>E-Mail<span>*</span></label><br>
                    <input type="text" class="text" name="email" required/>
                </div>
                <div class="field_item">
                    <label>Gender</label><br>
                    <select name="gender">
                        <option value="1">Male</option>
                        <option value="2">Female</option>
                    </select>
                </div>
                <div class="field_item">
                    <label>Date of Birth</label><br>
                    <select name="birth_day">
                        {% for day in days %}
                        <option value="{{day}}">{{day}}</option>
                        {% endfor %}
                    </select>
                    <select name="birth_month">
                        {% for month in months %}
                        <option value="{{month.id}}">{{month.name}}</option>
                        {% endfor %}
                    </select>
                    <select name="birth_year">
                        {% for year in years %}
                        <option value="{{year}}">{{year}}</option>
                        {% endfor %}
                    </select>
                </div>
                <div class="field_item">
                    <label>TIN</label><br>
                    <input type="text" class="text" name="tin"/>
                </div>
                <div class="field_item">
                    <label>Passport</label><br>
                    <input type="text" class="text" name="passport"/>
                </div>                        
                <div class="field_item">
                    <label>Driver's License</label><br>
                    <input type="text" class="text" name="drivers_license"/>
                </div>                        
                <div class="field_item">
                    <label>Marital Status</label><br>
                    <select name="marital_status">
                        <option value="1">Single</option>
                        <option value="2">Married</option>
                        <option value="3">Divorced</option>
                        <option value="4">Widowed</option>                            
                    </select>
                </div>
                <div class="field_item">
                    <input type="text" class="text" name="spouse_name" placeholder="Name Of Spouse"/><input type="text" class="text" name="spouse_contact_no" placeholder="Contact No."/><br>
                </div>                                                
                <div class="field_item_div">
                    <div class="normal_label"><label>Beneficiaries</label></div>
                </div>
                <div class="field_item">
                    <input type="text" class="text" name="beneficiary1_name" placeholder="Name"/><input type="text" class="text" name="beneficiary1_relationship" placeholder="Relationship"/><input type="text" class="text" name="beneficiary1_contact_no" placeholder="Contact No."/><br>
                    <input type="text" class="text" name="beneficiary2_name" placeholder="Name"/><input type="text" class="text" name="beneficiary2_relationship" placeholder="Relationship"/><input type="text" class="text" name="beneficiary2_contact_no" placeholder="Contact No."/><br>
                    <input type="text" class="text" name="beneficiary3_name" placeholder="Name"/><input type="text" class="text" name="beneficiary3_relationship" placeholder="Relationship"/><input type="text" class="text" name="beneficiary3_contact_no" placeholder="Contact No."/><br>
                </div>                        
                <div class="field_item">
                    <label>Office Address</label><br>
                    <input type="text" class="text" name="office_street" placeholder="Street" size="50"/>
                    <input type="text" class="text" name="office_city" placeholder="City"/><br>
                    <input type="text" class="text" name="office_state" placeholder="State"/><br>
                    <select name="office_country">
                     <option value="PH">Philippines</option>
                 </select>
             </div>
             <div class="field_item">
                <label>Shipping Address</label><br>
                <input type="text" class="text" name="shipping_street" placeholder="Street" size="50"/>
                <input type="text" class="text" name="shipping_city" placeholder="City"/><br>
                <input type="text" class="text" name="shipping_state" placeholder="State"/><br>
                <select name="shipping_country">
                 <option value="PH">Philippines</option>
             </select>
             <input type="text" class="text" name="zipcode" placeholder="Zip Code">
         </div>
         <div class="field_item_div">
            <div class="normal_label"><label>Other Contact</label></div>
        </div>
        <div class="field_item">
            <input type="text" class="text" name="facebook_id" placeholder="Facebook ID "/>
            <input type="text" class="text" name="skype_id" placeholder="Skype ID"/>
            <input type="text" class="text" name="twitter_id" placeholder="Twitter ID"/>
        </div>
        <div class="field_item">
            <label>Member Of Other MLM Company?</label>
            <input type="checkbox" name="member_of_other_mlm"/><br>
        </div>
        <div class="field_item_div">
            <div class="normal_label"><label>Where to Deposit Your Commissions</label></div>
        </div>
        <div class="field_item">
            <label>Bank Details</label><br>
            <input type="text" class="text" name="bank_name" placeholder="Bank Name"/>
            <input type="text" class="text" name="bank_account_name" placeholder="Bank Account Name"/>
            <input type="text" class="text" name="bank_account_no" placeholder="Bank Account Number"/>
            <input type="text" class="text" name="bank_branch" placeholder="Branch Name"/>
        </div>
        <div class="field_item_div">
            <div class="normal_label"><label>Sponsor Information</label></div>
        </div>                        

        <div class="field_item">
            <label>Sponsor<span>*</span></label><br>
            <input type="text" id="tbSponsor" onKeyUp="displaySponsor()" class="text" name="sponsor" required/>
            <div id='displayName'></div>
        </div>
        <div class="field_item">
            <label>Activation Code<span>*</span></label><br>
            <input type="text" class="text" name="activation_code" required/>
        </div>
        <div class="field_item">
            <br>
            <br>
            <input type="checkbox" name="agree"/>
            <label>I agree to the <a href="{{root}}help/terms-and-condition" target="_blank">Terms Of Service.</a></label>
        </div>
        <div class="field_item"><button type="submit" name="submit" class="form_button">Register</button></div>
    </fieldset>
</form>
</div>
</div>

<script type="text/javascript">
function displaySponsor(){
    var dDisplayName= document.getElementById("displayName"); 
    var idList = [];
    var userList = [];
    var inputID = document.getElementById('tbSponsor').value;

    {% for id in idList %}
    idList.push("{{id.id}}");
    userList.push("{{id.fullname}}");
    {%endfor%}

    var indexInput= idList.indexOf(inputID);

    if(indexInput!=-1){
        dDisplayName.innerHTML= userList[indexInput];
    }else{
        dDisplayName.innerHTML= "INVALID SPONSOR";
    }
}
</script>
{% endblock content %}