{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Member Genealogy</h1>
    <div id="members">
        <div class="subcontent">
            <span class="subcontent_heading">Genealogy for {{member_id}}</span>
            <br>
            <br>
            <div class="toprow">
                <div class="topcolumn"><b>Level</b></div>
                <div class="topcolumn"><b>Member</b></div>
                <div class="topcolumn"><b>Date Joined</b></div>
                <div class="topcolumn"><b>Points Earned</b></div>
            </div>
            {% for item in members %}
            <div class="row">
                <div class="column">{{item.level}}</div>
                <div class="column">{{item.name}}</div>
                <div class="column">{{item.date}}</div>
                <div class="column">{{item.points}}</div>
            </div>
            {% endfor %}
            <div id="pages">
                <div id="page_info">Showing Pages {{current_page}} of {{num_pages}}</div>
                <div id="page">First</div>
                <div id="page">Prev</div>
                <div id="page">1</div>
                <div id="page">2</div>
                <div id="page">3</div>
                <div id="page">4</div>
                <div id="page">5</div>
                <div id="page">Next</div>
                <div id="page">Last</div>                
            </div>
        </div>
    </div>
</div>
{% endblock content %}