{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Products</h1>
    <div class="common_table_container">
        <div class="subcontent">
            <span class="subcontent_heading">Product Category List</span>
            <br>
            <br>
            <div style="float: left;">
            </div>                                            
            <div style="float: right;">
                <a href="{{root}}admin/products/type/category/action/add" id="add_product" class="tabulate">Add New Category</a>
            </div>                                
            <div class="toprow">
                <div class="topcolumn"><b>Category Id</b></div>
                <div class="topcolumn"><b>Title</b></div>
                <div class="topcolumn"><b>Parent</b></div>
                <div class="topcolumn"></div>                
            </div>
            {% for item in categories %}
            <div class="row">
                <div class="column">{{item.id}}</div>
                <div class="column">{{item.title}}</div>
                <div class="column">{{item.parent}}</div>
                <div class="column">
                    {% if item.id %}
                    <a href="{{root}}admin/products/manage/delete/{{item.id}}">
                        <img src="{{root}}images/icons/icon_delete.png" width="24"/>
                    </a>
                    <a href="{{root}}admin/products/view/{{item.id}}">
                        <img src="{{root}}images/icons/icon_edit.png" width="24"/>
                    </a>
                    {% endif %}
                </div>
            </div>
            {% endfor %}
            <div id="pages">
                <div id="page_info">Showing Pages {{current_page}} of {{num_pages}}</div>
                <div id="page">First</div>
                <div id="page">Prev</div>
                <div id="page">1</div>
                <div id="page">2</div>
                <div id="page">3</div>
                <div id="page">4</div>
                <div id="page">5</div>
                <div id="page">Next</div>
                <div id="page">Last</div>                
            </div>
        </div>
    </div>
</div>
<script language="javascript">
$("#product_select_category").change(function() {
    var product_id = $(this).val();
    alert(product_id);
    if (product_id != 0) {
        window.location = "{{root}}admin/products/category/" + product_id;
    }
});
</script>
{% endblock content %}