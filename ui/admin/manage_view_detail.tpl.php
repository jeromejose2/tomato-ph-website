{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">User Details</h1>
    <div class="order_detail_container" align="center">
        <form id="update_user_detail_form" action="{{root}}admin/manage/action/update" method="post">
            <fieldset>
                <div class="form_item">
                    <div class="form_label"><label>Username: </label></div>
                    <div><input type="text" class="text" value="{{user}}" readonly></div>
                </div>
                <div class="form_item">
                    <div class="form_label"><label>Role: </label></div>
                    <div><input type="text" class="text" value="{{role}}" readonly></div>
                </div>
                <div class="form_item">
                    <div class="form_label"><label>Fullname: </label></div>
                    <div><input type="text" id="fullname" name="fullname" class="text" value="{{fullname}}" readonly></div>
                </div>
                <div class="form_item">
                    <div class="form_label"><label>E-mail: </label></div>
                    <div><input type="text" id="email" name="email" class="text" value="{{email}}" readonly></div>
                </div>
                <div class="form_item">
                    <div class="form_label"><label>Status: </label></div>
                    <div><input type="text" class="text" value="{{status}}" readonly></div>
                </div>
                <div class="form_item">
                    <div class="form_label"><label>Date Created: </label></div>
                    <div><input type="text" id="" name="" class="text" value="{{creation_date}}" readonly></div>
                </div>
                <div class="form_item">
                    <div class="form_label"><label>Created By: </label></div>
                    <div><input type="text" id="" name="" class="text" value="{{created_by}}" readonly></div>
                </div>
                <div class="form_item">
                    <div><button type="button" class="form_button">Edit</button><button type="button" class="form_button">Change Password</button></div>
                </div>                
            </fieldset>
        </form>    
    </div>
</div>
{% endblock content %}