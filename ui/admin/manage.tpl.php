{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Manage Users</h1>
    <div class="common_table_container">
        <div class="subcontent">
            <span class="subcontent_heading">User List</span>
            <br>
            <br>
            <div class="form_item">
                <div class="form_label"><label>Category: </label></div>
                <div>
                    {% if user_types %}
                    <select id="user_category_filter" name="user_type" class="select_item">
                        <option value="0">All Users</option>
                        {% for item in user_types %}
                        <option value="{{item.id}}">{{item.name}}</option>
                        {% endfor %}
                    </select>
                    {% endif %}
                    <input type="hidden" id="url" value="{{root}}admin/manage/user_type/">
                </div>
            </div>            
            <div style="float: right;">
                <a href="{{root}}admin/manage/action/addnew" class="tabulate">Add New User</a>
                <a href="{{root}}admin/manage/action/search" class="tabulate">Search</a>
            </div>
            <div class="toprow">
                <div class="name"><b>Name</b></div>
                <div class="name"><b>User</b></div>
                <div class="name"><b>Role</b></div>
                <div class="date"><b>Date</b></div>
                <div class="name"><b>Creator</b></div>
                <div class="status"><b>Status</b></div>
                <div class="topcolumn"></div>                
            </div>
            {% for item in users %}
            <div class="row">
                <div class="name">{{item.name}}</div>
                <div class="name">{{item.user}}</div>
                <div class="name">{{item.role}}</div>
                <div class="date">{{item.date}}</div>
                <div class="name">{{item.creator}}</div>
                <div class="status">{{item.status}}</div>
                <div class="column">
                    {% if item.name %}
                    <a href="{{root}}admin/manage/action/ban/id/{{item.id}}">
                        <img src="{{root}}images/icons/icon_ban.png" width="24" alt="Ban User"/>
                    </a>
                    <a href="{{root}}admin/manage/action/delete/id/{{item.id}}">
                        <img src="{{root}}images/icons/icon_delete.png" width="24" alt="Delete User"/>
                    </a>
                    <a href="{{root}}admin/manage/action/view/id/{{item.id}}">
                        <img src="{{root}}images/icons/icon_edit.png" width="24" alt="Edit/View Info"/>
                    </a>
                    {% endif %}
                </div>
            </div>
            {% endfor %}
            <div class="pagination">
                <div class="info">Showing Pages {{current_page}} of {{num_pages}}</div>
                <div class="pages">
                    <div class="item">
                        {% if current_page == first %}
                            <a href="#" class="page disable">First</a>
                        {% else %}
                            <a href="{{root}}admin/manage/page/{{first}}/user_type/{{user_type}}" class="page gradient">First</a>
                        {% endif %}
                    </div>
                    {% if current_page == first %}
                        <div class="item"><a href="#" class="page disable">Prev</a></div>
                    {% else %}
                        <div class="item"><a href="{{root}}admin/manage/page/{{prev}}/user_type/{{user_type}}" class="page gradient">Prev</a></div>
                    {% endif %}
                    {% for page in pages %}
                    <div class="item">
                        {% if page.show == 1 %}
                            {% if page.id == current_page %}
                                <a href="{{root}}admin/manage/page/{{page.id}}/user_type/{{user_type}}" class="page active">{{page.id}}</a>
                            {% else %}
                                <a href="{{root}}admin/manage/page/{{page.id}}/user_type/{{user_type}}" class="page gradient">{{page.id}}</a>
                            {% endif %}
                        {% else %}
                            <a href="#" class="page disable">{{page.id}}</a>
                        {% endif %}
                    </div>
                    {% endfor %}
                    {% if current_page == last %}
                        <div class="item"><a href="#" class="page disable">Next</a></div>
                        <div class="item"><a href="#" class="page disable">Last</a></div>   
                    {% else %}
                        <div class="item"><a href="{{root}}admin/manage/page/{{next}}/user_type/{{user_type}}" class="page gradient">Next</a></div>
                        <div class="item"><a href="{{root}}admin/manage/page/{{last}}/user_type/{{user_type}}" class="page gradient">Last</a></div>
                    {% endif %}
                </div>
            </div>            
        </div>
    </div>
</div>
{% endblock content %}