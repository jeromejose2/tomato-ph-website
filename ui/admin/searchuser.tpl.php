{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Add New User</h1>
    <br>
    <br>
    <br>
    <div class="order_detail_container" align="center">
        <form action="{{root}}admin/manage/action/search" method="post">
            <fieldset>
                <div class="form_item">
                    <div class="form_label"><label>Username:</label></div>
                    <div><input type="text" class="text" name="user" value="" maxlength="32"/></div>
                </div>
                <div class="form_item">
                    <div class="form_label">&nbsp;</div>
                    <div><button type="submit" class="form_button">Search</button></div>
                </div> 
            </fieldset>
        </form>        
    </div>
</div>
{% endblock content %}