{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Customer Orders</h1>
    <div class="common_table_container">
        <div class="subcontent">
            <span class="subcontent_heading">Orders</span>
            <br>
            <br>
            <div class="toprow">
                <div class="number">Id</div>
                <div class="name">Customer Name</div>
                <div class="mail">E-mail</div>
                <div class="date">Purchased On</div>                
                <div class="number">Sub Total</div>
                <div class="number">Total</div>
                <div class="number">Discount</div>
                <div class="status">Status</div>
                <div class="short_name">Details</div>              
            </div>
            {% for item in orders %}
            <div class="row">
                <div class="number">{{item.id}}</div>
                <div class="name">{{item.customer_name}}</div>
                <div class="mail">{{item.email}}</div>
                <div class="date">{{item.order_datetime}}</div>                
                <div class="number">&#x20b1; {{item.subtotal}}</div>
                <div class="number">&#x20b1; {{item.total}}</div>
                <div class="number">&#x20b1; {{item.discount}}</div>
                <div class="status">{{item.status}}</div>
                <div class="short_name"><a href="{{root}}admin/orders/action/view/type/detail/orderid/{{item.id}}" class="button">View</a></div>
            </div>
            {% endfor %}
            <div class="pagination">
                <div class="info">Showing Pages {{current_page}} of {{num_pages}}</div>
                <div class="pages">
                    <div class="item">
                        {% if current_page == first or prev == first %}
                            <a href="#" class="page disable">First</a>
                        {% else %}
                            <a href="{{root}}admin/orders/page/{{first}}" class="page gradient">First</a>
                        {% endif %}
                    </div>
                    {% if current_page == first or prev == first %}
                        <div class="item"><a href="#" class="page disable">Prev</a></div>
                    {% else %}
                        <div class="item"><a href="{{root}}admin/orders/page/{{prev}}" class="page gradient">Prev</a></div>
                    {% endif %}
                    {% for page in pages %}
                    <div class="item">
                        {% if page.show == 1 %}
                            {% if page.id == current_page %}
                                <a href="{{root}}admin/orders/page/{{page.id}}" class="page active">{{page.id}}</a>
                            {% else %}
                                <a href="{{root}}admin/orders/page/{{page.id}}" class="page gradient">{{page.id}}</a>
                            {% endif %}
                        {% else %}
                            <a href="#" class="page disable">{{page.id}}</a>
                        {% endif %}
                    </div>
                    {% endfor %}
                    {% if current_page == last or next == last %}
                        <div class="item"><a href="#" class="page disable">Next</a></div>
                        <div class="item"><a href="#" class="page disable">Last</a></div>   
                    {% else %}
                        <div class="item"><a href="{{root}}admin/orders/page/{{next}}" class="page gradient">Next</a></div>
                        <div class="item"><a href="{{root}}admin/orders/page/{{last}}" class="page gradient">Last</a></div>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
</div>
{% endblock content %}