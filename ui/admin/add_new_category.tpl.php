{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Add New Category</h1>
    <br>
    <br>
    <form action="{{root}}admin/products/type/category/action/add" method="post" enctype="multipart/form-data" accept-charset="utf-8">
    <div id="members">
        <div class="form_item">
            <div class="form_label"><label>Category Name: </label></div>
            <div><input type="text" class="text" name="name" size="20" value=""/></div>
        </div>
        <div class="form_item">
            <button type="submit" class="form_button">Save</button>
        </div>        
    </div>
    </form>
</div>
{% endblock content %}