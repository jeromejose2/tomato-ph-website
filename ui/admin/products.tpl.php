{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Products</h1>
    <div class="common_table_container">
        <div class="subcontent">
            <span class="subcontent_heading">Product List</span>
            <br>
            <br>
            <div style="float: left;">
            {% if product_types %}
            <span class="category_label">Category: </span><select id="product_select_category" name="product_type" class="select_item">
                <option value="0">All Categories</option>
                {% for item in product_types %}
                <option value="{{item.id}}">{{item.title}}</option>
                {% endfor %}
            </select>
            {% endif %}
            </div>                                            
            <div style="float: right;">
                <a href="{{root}}admin/products/type/category/action/manage" id="add_product" class="tabulate">Manage Product Categories</a>
                <a href="{{root}}admin/products/type/product/action/add" id="add_product" class="tabulate">Add New Product</a>
            </div>                                
            <div class="toprow">
                <div class="topcolumn"><b>Product Id</b></div>
                <div class="topcolumn"><b>Photo</b></div>
                <div class="topcolumn"><b>Name</b></div>
                <div class="topcolumn"><b>Price</b></div>
                <div class="topcolumn"><b>Discount</b></div>
                <div class="topcolumn"><b>In Stock</b></div>                
                <div class="topcolumn"></div>                
            </div>
            {% for item in products %}
            <div class="row">
                <div class="column_with_pic">{{item.id}}</div>
                <div class="column_with_pic">
                    {% if item.photo %}
                    <img src="{{root}}{{item.photo}}" height="110">
                    {% endif %}
                </div>
                <div class="column_with_pic">{{item.name}}</div>
                <div class="column_with_pic">{{item.price}}</div>
                <div class="column_with_pic">{{item.discount}}</div>                
                <div class="column_with_pic">{{item.in_stock}}</div>                
                <div class="column_with_pic">
                    {% if item.id %}
                    <a href="{{root}}admin/products/type/product/action/delete/id/{{item.id}}">
                        <img src="{{root}}images/icons/icon_delete.png" width="24" alt="Delete User"/>
                    </a>
                    <a href="{{root}}admin/products/type/product/action/view/id/{{item.id}}">
                        <img src="{{root}}images/icons/icon_edit.png" width="24" alt="Edit/View Info"/>
                    </a>
                    {% endif %}
                </div>
            </div>
            {% endfor %}
            <div class="pagination">
                <div class="info">Showing Pages {{current_page}} of {{num_pages}}</div>
                <div class="pages">
                    <div class="item">
                        {% if current_page == first %}
                            <a href="#" class="page disable">First</a>
                        {% else %}
                            <a href="{{root}}admin/products/page/{{first}}" class="page gradient">First</a>
                        {% endif %}
                    </div>
                    {% if current_page == first %}
                        <div class="item"><a href="#" class="page disable">Prev</a></div>
                    {% else %}
                        <div class="item"><a href="{{root}}admin/orders/products/{{prev}}" class="page gradient">Prev</a></div>
                    {% endif %}
                    {% for page in pages %}
                    <div class="item">
                        {% if page.show == 1 %}
                            {% if page.id == current_page %}
                                <a href="{{root}}admin/orders/products/{{page.id}}" class="page active">{{page.id}}</a>
                            {% else %}
                                <a href="{{root}}admin/orders/products/{{page.id}}" class="page gradient">{{page.id}}</a>
                            {% endif %}
                        {% else %}
                            <a href="#" class="page disable">{{page.id}}</a>
                        {% endif %}
                    </div>
                    {% endfor %}
                    {% if current_page == last %}
                        <div class="item"><a href="#" class="page disable">Next</a></div>
                        <div class="item"><a href="#" class="page disable">Last</a></div>   
                    {% else %}
                        <div class="item"><a href="{{root}}admin/products/page/{{next}}" class="page gradient">Next</a></div>
                        <div class="item"><a href="{{root}}admin/products/page/{{last}}" class="page gradient">Last</a></div>
                    {% endif %}
                </div>
            </div>            
        </div>
    </div>
</div>
<script language="javascript">
$("#product_select_category").change(function() {
    var product_id = $(this).val();
    alert(product_id);
    if (product_id != 0) {
        window.location = "{{root}}admin/products/category/" + product_id;
    }
});
</script>
{% endblock content %}