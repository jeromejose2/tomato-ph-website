{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Success</h1>
    <br>
    <br>
    <div>
        <span>User have been created successfully!</span>
    </div>
</div>
{% endblock content %}