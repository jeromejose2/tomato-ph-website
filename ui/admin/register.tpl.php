{% extends "index.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include 'admin_nav.tpl.php' %}
<script language="javascript" type="text/javascript">
			
    $(document).on('change', '.quantity', function(){
        var total = 0;
        $('.quantity').each(function() {
            total += parseInt($(this).val());
        });
        $('#sum').val(total)
    });
</script>
		
<div id="cont-wrap">
	<h1 class="page-title">REGISTRATION FORM</h1>
	<div id="search">
        <center>
        <font siz="2" face="Verdana" color ="#333333"><b>SEARCH SPONSORS HERE</b></font><br/>
		<form action="" method="POST" name="search">
			<input type="text" size="50" placeholder="Search here.." name="search" class="text long-txtfld"/>&nbsp;
			<input type="image" src="{{root}}icons/search.png" name="search">
		</form>
        </center>
	</div>
	<form action="register/step/2" method="POST" name="register">
		<font face="Verdana" size=1 color="red"><center>All fields are required</center></font>
        <br>
        <br>
		<table class="roboto">
            <tr>
			     <td>Sponsor:</td><td><input type="text" name="sponsor" class="mem-txtfld"></td>
			     <td>Employer:</td><td><input type="text" name="employer" class="mem-txtfld"></td>
			</tr>
            <tr>
                <td>Username:</td>
                <td><input type="text" name="username" class="long-txtfld"></td>
            </tr>
            <tr>
			    <td>Password:</td>
                <td><input type="password" name="password" class="long-txtfld"></td>
			</tr>
            <tr>
                <td>Confirm Password:</td>
                <td><input type="password" name="confirm_password" class="long-txtfld"></td>
			</tr>
            <tr>
                <td>First Name:</td>
                <td><input type="text" name="firstname" class="long-txtfld"></td>
			</tr>
            <tr>
                <td>Last Name:</td>
                <td><input type="text" name="lastname" class="long-txtfld"></td>
			</tr>
            <tr>
                <td>Middle Name:</td>
                <td><input type="text" name="middlename" class="long-txtfld"></td>
			</tr>
            <tr>
                <td>Address:</td>
                <td><input type="text" name="address" class="long-txtfld"></td>
			</tr>
            <tr>
                <td>Landline No:</td>
                <td><input type="text" name="landline" class="long-txtfld"></td>
			</tr>
            <tr>
                <td>Mobile No:</td>
                <td><input type="text" name="mobile" class="long-txtfld"></td>
			</tr>            
            <tr>
                <td>Gender:</td>
                <td>
                    <input type="radio" name="gender" value="Male">Male&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="gender" value="Female">Female
                </td>
            </tr>
            <tr>
			<td>Birthdate:</td><td>
				<select name="month" class="reg-drpdwn">
					<option>--</option>
					<option value="1">January</option>
					<option value="2">February</option>
					<option value="3">March</option>
					<option value="4">April</option>
					<option value="5">May</option>
					<option value="6">June</option>
					<option value="7">July</option>
					<option value="8">August</option>
					<option value="9">September</option>
					<option value="10">October</option>
					<option value="11">November</option>
					<option value="12">December</option>
				</select> 
				<select name="day" class="reg-drpdwn">
    			    <option>--</option>
                    {% for d in day %}
    			    <option value="{{d}}">{{d}}</option>
                    {% endfor %}
    			</select> 
				<select name="year" class="reg-drpdwn">
					<option>--</option>
                    {% for y in year %}
    				<option value="{{y}}">{{y}}</option>
                    {% endfor %}
				</select>
			</td>
            </tr>
            <tr>
			<td>E-mail Address:</td><td><input type="text" name="email" class="long-txtfld"></td>
            </tr>
            <tr>
                <td><input type="button" name="select_kit" onclick="location='#kit'" value="Select Kit"></td><td></td>
            </tr>
            <tr>
                <td><input type="button" name="select_product" onclick="location='#products'" value="Select Products"></td><td></td>
            </tr>            
        </table>
        <br>
		<div id="kit" class="modalDialog">
            <div>
                <a href="#close">X</a>
                <table class="roboto" align = 'center'>				 
                    <tr>
                        <th style='height:50px;' width=100><center>IMAGE</center></th>
                        <th style='height:50px;' width=100><center>NAME</center></th>
                        <th style='height:50px;' width=150><center>DESCRIPTION</center></th>
                        <th style='height:50px;' width=100><center>PRICE</center></th>
                        <th style='height:50px;' width=250><center>ENTER PIN & CODE NUMBER<br><font face="Verdana" size=1 color="red">(required)</font></center></th>
                    </tr>
                    {% for item in kit %}
                    <tr>
                        <td style='overflow:hidden; width:100px;' align='center'><img src="{{root}}images/{{item.image}}" width=100 height=100></td>
                        <td style='overflow:hidden; width:100px;' align='center'>{{item.name}}</td>
                        <td style='overflow:hidden; width:150px;' align='center'>{{item.desc}}</td>
                        <td style='overflow:hidden; width:100px;' align='center'>{{item.price}}</td>
                        <td style='overflow:hidden; width:250px;' align='center'>
                            <table class = 'hovertable'>
                                <tr><td><font face='Verdana' size='2'>PIN #:</td><td><input type='password' name ='pin'></td></tr>
                                <tr><td><font face='Verdana' size='2'>CODE #:</td><td><input type='password' name ='code'></td>
                            </table>
                        </td>
                    </tr>
                    {% endfor %}
                </table>
            </div>
        </div>
        <div id="products" class="modalDialog">
            <div style="max-height:100%;overflow:auto;">
                <a href="#close">X</a>
        <table class="roboto" align = 'center'>				 
			<tr>
                <th style='height:50px;' width=100><center>PRODUCT ID</center></th>
                <th style='height:50px;' width=100><center>NAME</center></th>
                <th style='height:50px;' width=150><center>DESCRIPTION</center></th>
                <th style='height:50px;' width=100><center>PRICE</center></th>
                <th style='height:50px;' width=100><center>STATUS</center></th>
                <th style='height:50px;' width=100><center>IMAGE</center></th>
                <th style='height:50px;' width=100><center>DATE ADDED</center></th>
                <th style='height:50px;' width=100><center>QUANTITY</center></th>
            </tr>
            {% for product in products %}
            <tr>
                <td style='overflow:hidden; width:100px;' align='center'>{{product.product_id}}</td>
				<td style='overflow:hidden; width:100px;' align='center'>{{product.name}}</td>
				<td style='overflow:hidden; width:150px;' align='center'>{{product.description}}</td>
				<td style='overflow:hidden; width:100px;' align='center'>{{product.price}}</td>
				<td style='overflow:hidden; width:100px;' align='center'>{{product.status}}</td>
				<td style='overflow:hidden; width:100px;' align='center'><img src ='{{root}}images/{{product.image}}' width='100' height='100'></td>
				<td style='overflow:hidden; width:100px;' align='center'>{{product.date_added}}</td>												
				<td width=100 align='center'>
                    <input type="text" name="quantity" value="0" maxlength="4" size="4" align="right"/>
                </td>
            </tr>
            {% endfor %}
            <tr>
                <td colspan=4></td>
                <td align='center'><b>TOTAL COST:</td>
                <td align='center'><input type='text' name='total' class='text' id='sum' size='10' readonly></td>
                <td align='center'><input type='reset' name='reset' value='RESET'></td>
                <td align='center'><input type='submit' name='submit' value='SUBMIT'></td>
            </tr>            
        </table>
            </div>
        </div>    
    </form>       
</div>    
{% endblock content %}