{% extends "index.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include 'admin_nav.tpl.php' %}
<div id="cont-wrap">
	<h1 class="page-title">Transactions</h1>
    <form action='' method='POST'>
        <input type='hidden' name='command' value='searchadmintransactions'>
        <table class = "members-table" align = "center">
            <th>USERNAME:<input type = "text" name = "username" class="mem-txtfld"></th>
            <th>OR DATE FROM:</th><th><input type = "date" name = "datefrom" class="mem-txtfld"></th>
            <th>TO:</th><th><input type = "date" name = "dateto" class="mem-txtfld"></th>
            <th>SEARCH&nbsp;<input type = 'image' src = '../icons/search.png' name = 'search'></th>
        </table>
    </form>
    <table class = 'members-table transactions' align = 'center' >
        <tr>
            <th width = 100>TRANSACTION ID</th>
            <th width = 100>DATE</th>
            <th width = 100>TOTAL COST</th>
            <th width = 100>BUYER</th>
            <th width = 100>VIEW DETAILS</th>
        </tr>
        {% for t in transactions %}    
        <tr>
            <td style='overflow:hidden; width:100px' align='center'>{{t.transaction_id}}</td>
            <td style='overflow:hidden; width:100px' align='center'>{{t.transaction_date}}</td>
            <td style='overflow:hidden; width:100px' align='center'>{{t.totalcost}}</td>
            <td style='overflow:hidden; width:100px' align='center'>{{t.buyer}}</td>
            <td style='overflow:hidden; width:100px' align='center'><a href='#'><img src = '{{root}}icons/browse.png'></a></td>
        </tr>
        {% endfor %}
    </table>
	<center><a href="viewshoptransactions.php" style="text-decoration:none"><font style="font-size:13px" face="Verdana" color="2B547E"><b>-- VIEW SHOP TRANSACTIONS --</b></font></a></center>
	</div>
{% endblock content %}