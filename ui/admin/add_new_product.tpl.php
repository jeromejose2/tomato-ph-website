{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Add New Product</h1>
    <br>
    <br>
    <form action="{{root}}admin/products/type/product/action/add" method="post" enctype="multipart/form-data" accept-charset="utf-8">
    <div id="members">
        <div class="form_item">
            <div class="form_label"><label>Product Name: </label></div>
            <div><input type="text" name="name" class="text" size="20" value=""/></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Product Type: </label></div>
            <div>
            {% if product_types %}
            <select name="product_type">
                {% for item in product_types %}
                <option value="{{item.id}}">{{item.title}}</option>
                {% endfor %}
            </select>
            {% endif %}                
            </div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Description: </label></div>
            <div><textarea name="description" class="text" cols="40" rows="6"></textarea></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Price: </label></div>
            <div><input type="text" name="price" class="text" size="20" value=""/></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Tax: </label></div>
            <div><input type="text" name="tax" class="text" size="20" value=""/></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Discount: </label></div>
            <div><input type="text" name="discount" class="text" size="20" value=""/></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Quantity: </label></div>
            <div><input type="text" name="quantity" class="text" size="20" value=""/></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Photo: </label></div>
            <div><input type="file" name="photo" file-accept="jpg, jpeg, png" file-maxsize="1024"/></div>
        </div>
        <div class="form_item">
            <button type="button" class="form_button">Save</button>
        </div>        
    </div>
    </form>
</div>
{% endblock content %}