{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
        <div id="cont-wrap">
            <h1 class="page-title">DASHBOARD</h1>
            <div class="common_table_container">
                <ul id="content_panel">
                    <li>
                        <h2>Welcome back, {{name}}!</h2>
                        <p>Your last login was <em>{{last_login}}</em></p>
                        <br>                        
                        <div class="subcontent">
                            <span class="subcontent_heading">Latest members</span>
                            <br>
                            <br>
                            <div class="toprow">
                                <div class="name"><b>Member</b></div>
                                <div class="name"><b>Sponsor</b></div>
                                <div class="date"><b>Date Joined</b></div>
                            </div>
                            {% for item in members %}
                            <div class="row">
                                <div class="name">{{item.name}}</div>
                                <div class="name">{{item.sponsor}}</div>
                                <div class="date">{{item.date}}</div>
                            </div>
                            {% endfor %}
                        </div>
                    </li>
                    <li>
                        <div class="normal_label">Total Users: {{num_users}}</div>
                        <br>
                        <div class="normal_child_label">New: {{new_users}}</div>  
                        <div class="normal_child_label">Pending: {{num_members}}</div>                                        
                        <br>
                        <div class="normal_label">Total Members: {{num_members}}</div>
                        <br>
                        <div class="normal_child_label">New: {{new_members}}</div>
                        <div class="normal_child_label">Pending: {{pending_members}}</div>
                        
                    </li>                
                </ul>
                <!--
                <br>
                <br>
                <img src="{{root}}images/members-line.png">
                <br>
                <br>
                <img src="{{root}}images/members-pie.png">
                <img src="{{root}}images/members-bar.png">
                -->
            </div>
        </div>
{% endblock content %}