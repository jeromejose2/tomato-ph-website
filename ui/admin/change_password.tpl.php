{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Settings</h1>
    <br>
    <br>
    <br>
    <br>    
    <div class="order_detail_container" align="center">
        <br>
        <br>
        <form action="{{root}}admin/account-settings/action/changepass" method="post">
            <fieldset>
                <div class="form_item">
                    <div class="form_label"><label>Old Password: </label></div>
                    <div><input type="password" class="text" name="oldpass" maxlength="32" required></div>
                </div>
                <div class="form_item">
                    <div class="form_label"><label>New Password: </label></div>
                    <div><input type="password" class="text" name="newpass" maxlength="32" required></div>
                </div>
                <div class="form_item">
                    <div class="form_label"><label>Confirm Password: </label></div>
                    <div><input type="password" class="text" name="confirmpass" maxlength="32" required></div>
                </div>
                <div class="form_item">
                    <div><button type="submit" class="form_button">Update</button></div>
                </div>
            </fieldset>    
        </form>
        <br>
        <br>
    </div>
</div>
{% endblock content %}