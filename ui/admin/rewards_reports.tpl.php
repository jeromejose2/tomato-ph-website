{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Rewards Reports</h1>
    <br>
    <br>
    <div class="order_detail_container">
        <div class="form_item">
            <div class="form_label"><label>Total Members:</label></div>
            <div><input type="text" class="text" value="{{total_members}}" readonly></div>
        </div>        
        <div class="form_item">
            <div class="form_label"><label>Total Members Joined This Month:</label></div>
            <div><input type="text" class="text" value="{{total_member_this_month}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Download Member List</label></div>
            <div><a href="{{root}}admin/rewards-reports/get/memberlist/as/pdf" class="button">Download PDF</a></div>
        </div>
        <div class="form_item">
            <div class="">&nbsp;</div>
            <div class="common_table_container">
                <div class="subcontent">
                    <span class="subcontent_heading">Top 10 Earning Members</span>
                    <br>
                    <br>
                    <div class="toprow">
                        <div class="number"><b>ID</b></div>
                        <div class="name"><b>Name</b></div>
                        <div class="number"><b>Earnings (SP)</b></div>
                        <div class="topcolumn"><b>&nbsp;</b></div>
                    </div>
                    {% for m in top_10_earning_members %}
                    <div class="row">
                        <div class="number">{{m.id}}</div>
                        <div class="name">{{m.name}}</div>
                        <div class="number">{{m.points}}</div>
                        <div class="column">&nbsp;</div>
                    </div>
                    {% endfor %}
                </div>
            </div>
        </div>        
        <div class="form_item_div">
            <div>&nbsp;</div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Total Sales (This Month):</label></div>
            <div><input type="text" class="text" value="&#x20b1; {{total_sales_current}}" readonly></div>
        </div>                
        <div class="form_item">
            <div class="form_label"><label>Total Sales (YTD):</label></div>
            <div><input type="text" class="text" value="&#x20b1; {{total_sales_ytd}}" readonly></div>
        </div>                
        <div class="form_item">
            <div class="">&nbsp;</div>
            <div class="common_table_container">
                <div class="subcontent">
                    <span class="subcontent_heading">Weekly Sales for the Month of {{current_month}}</span>
                    <br>
                    <br>
                    <div class="toprow">
                        <div class="short_name"><b>Week</b></div>
                        <div class="number"><b>Total Price</b></div>
                        <div class="number"><b>Total Items Sold</b></div>
                        <div class="topcolumn"><b>&nbsp;</b></div>
                    </div>
                    {% for m in current_month_sales %}
                    <div class="row">
                        <div class="short_name">{{m.week}}</div>
                        <div class="number">&#x20b1; {{m.total_price}}</div>
                        <div class="number">{{m.total_items}}</div>
                        <div class="column">&nbsp;</div>
                    </div>
                    {% endfor %}
                    <div id="pages" style="text-align: right;"><h2>Total &#x20b1; {{total_sales}}</h2></div>                    
                </div>
            </div>
        </div>
        <div class="form_item">
            <div class="">&nbsp;</div>
            <div class="common_table_container">
                <div class="subcontent">
                    <span class="subcontent_heading">Sales per Month</span>
                    <br>
                    <br>
                    <div class="toprow">
                        <div class="short_name"><b>Month</b></div>
                        <div class="number"><b>Total Price</b></div>
                        <div class="number"><b>Total Items Sold</b></div>
                        <div class="topcolumn"><b>&nbsp;</b></div>
                    </div>
                    {% for m in per_month_sales %}
                    <div class="row">
                        <div class="short_name">{{m.month_name}}</div>
                        <div class="number">&#x20b1; {{m.total_price}}</div>
                        <div class="number">{{m.total_items}}</div>
                        <div class="column">&nbsp;</div>
                    </div>
                    {% endfor %}
                    <div id="pages" style="text-align: right;"><h2>Total &#x20b1; {{total_sales_per_month}}</h2></div>       
                                    <br>
                <br>
                <img src="{{root}}images/current-sales-per-month.png">

                </div>
            </div>
        </div>        
        <div class="form_item">
            <div class="">&nbsp;</div>
            <div class="common_table_container">
                <div class="subcontent">
                    <span class="subcontent_heading">Sales per Year</span>
                    <br>
                    <br>
                    <div class="toprow">
                        <div class="short_name"><b>Year</b></div>
                        <div class="number"><b>Total Price</b></div>
                        <div class="number"><b>Total Items Sold</b></div>
                        <div class="topcolumn"><b>&nbsp;</b></div>
                    </div>
                    {% for m in per_year_sales %}
                    <div class="row">
                        <div class="short_name">{{m.purchase_year}}</div>
                        <div class="number">&#x20b1; {{m.total_price}}</div>
                        <div class="number">{{m.total_items}}</div>
                        <div class="column">&nbsp;</div>
                    </div>
                    {% endfor %}
                    <div id="pages" style="text-align: right;"><h2>Total &#x20b1; {{total_sales_per_year}}</h2></div>                    
                </div>
            </div>
        </div>                
        <div class="form_item_div">
            <div>&nbsp;</div>
        </div>        
        <div class="form_item">
            <div class="form_label"><label>Total Products:</label></div>
            <div><input type="text" class="text" value="{{total_products}}" readonly></div>
        </div>           
        <div class="form_item">
            <div class="form_label"><label>Download Product List:</label></div>
            <div><a href="{{root}}admin/rewards-reports/get/productlist/as/pdf" class="button">Download PDF</a></div>
        </div>
        <div class="form_item">
            <div class="">&nbsp;</div>
            <div class="common_table_container">
                <div class="subcontent">
                    <span class="subcontent_heading">Top 10 Saleable Products</span>
                    <br>
                    <br>
                    <div class="toprow">
                        <div class="number"><b>ID</b></div>
                        <div class="name"><b>Product Name</b></div>
                        <div class="number"><b>Items Sold</b></div>
                        <div class="topcolumn"><b>&nbsp;</b></div>
                    </div>
                    {% for m in top_10_saleable_products %}
                    <div class="row">
                        <div class="number">{{m.id}}</div>
                        <div class="name">{{m.name}}</div>
                        <div class="number">{{m.total_items}}</div>
                        <div class="column">&nbsp;</div>
                    </div>
                    {% endfor %}
                </div>
            </div>
        </div>        
        <div class="form_item_div">
            <div>&nbsp;</div>
        </div>           
        <div class="form_item">
            <div class="form_label"><label>Download Order List:</label></div>
            <div><a href="{{root}}admin/rewards-reports/get/orderlist/as/pdf" class="button">Download PDF</a></div>
        </div>        
    </div>
</div>
{% endblock content %}