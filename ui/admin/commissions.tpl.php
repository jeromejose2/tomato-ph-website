{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Commissions</h1>
    <div class="common_table_container">
        <div class="subcontent">
            <span class="subcontent_heading">Commissions</span>
            <br>
            <br>
            <div class="toprow">
                <div class="name">Name</div>
                <div class="date">From</div>
                <div class="date">To</div>
                <div class="number">Tomato Brands</div>
                <div class="number">Other Brands</div>
                <div class="number">Points</div>
                <div class="number">Commission</div>
            </div>
            {% for item in commissions %}
            <div class="row">
                <div class="name">{{item.member_name}}</div>
                <div class="date">{{item.start_date}}</div>
                <div class="date">{{item.end_date}}</div>
                <div class="number">{% if item.member_name %}&#x20b1; {{item.tomato}}{% endif %}</div>
                <div class="number">{% if item.member_name %}&#x20b1; {{item.other}}{% endif %}</div>                
                <div class="number">{% if item.member_name %}SP {{item.points}}{% endif %}</div>
                <div class="number">{% if item.member_name %}&#x20b1; {{item.income}}{% endif %}</div>
            </div>
            {% endfor %}
            <div class="pagination">
                <div class="info">Showing Pages {{current_page}} of {{num_pages}}</div>
                <div class="pages">
                    <div class="item">
                        {% if current_page == first %}
                            <a href="#" class="page disable">First</a>
                        {% else %}
                            <a href="{{root}}admin/commissions/page/{{first}}" class="page gradient">First</a>
                        {% endif %}
                    </div>
                    {% if current_page == first %}
                        <div class="item"><a href="#" class="page disable">Prev</a></div>
                    {% else %}
                        <div class="item"><a href="{{root}}admin/commissions/page/{{prev}}" class="page gradient">Prev</a></div>
                    {% endif %}
                    {% for page in pages %}
                    <div class="item">
                        {% if page.show == 1 %}
                            {% if page.id == current_page %}
                                <a href="{{root}}admin/commissions/page/{{page.id}}" class="page active">{{page.id}}</a>
                            {% else %}
                                <a href="{{root}}admin/commissions/page/{{page.id}}" class="page gradient">{{page.id}}</a>
                            {% endif %}
                        {% else %}
                            <a href="#" class="page disable">{{page.id}}</a>
                        {% endif %}
                    </div>
                    {% endfor %}
                    {% if current_page == last or last == 0 %}
                        <div class="item"><a href="#" class="page disable">Next</a></div>
                        <div class="item"><a href="#" class="page disable">Last</a></div>   
                    {% else %}
                        <div class="item"><a href="{{root}}admin/commissions/page/{{next}}" class="page gradient">Next</a></div>
                        <div class="item"><a href="{{root}}admin/commissions/page/{{last}}" class="page gradient">Last</a></div>
                    {% endif %}
                </div>
            </div>            
        </div>
    </div>
</div>
{% endblock content %}