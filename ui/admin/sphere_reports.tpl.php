{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Sphere Reports</h1>
    <br>
    <br>
    <div id="members">
        <div class="subcontent">
            <span class="subcontent_heading">Sales</span>
            <br>
            <br>
            <div>
                <div style="float: right;">
                    <a href="#sales_modal" id="sales" class="tabulate">Sales</a>
                    <a href="#saleable_products_modal" id="saleable_products" class="tabulate">Saleable Products</a>
                    <a href="#commissions_modal" id="commissions" class="tabulate">Commissions</a>
                    <a href="#recruits_and_members_modal" id="recruits_and_members" class="tabulate">Recruits & Members</a>
                </div>
            </div>            
            <div class="toprow">
                <div class="topcolumn"><b>Member</b></div>
                <div class="topcolumn"><b>Sponsor</b></div>
                <div class="topcolumn"><b>Date Joined</b></div>
                <div class="topcolumn"><b>Points Earned</b></div>
                <div class="topcolumn"><b>Status</b></div>
                <div class="topcolumn"><b>Referal Code</b></div>
                <div class="topcolumn"></div>                
            </div>
            {% for item in members %}
            <div class="row">
                <div class="column">{{item.name}}</div>
                <div class="column">{{item.sponsor}}</div>
                <div class="column">{{item.date}}</div>
                <div class="column">{{item.points}}</div>
                <div class="column">{{item.status}}</div>
                <div class="column">{{item.referal_code}}</div>
                <div class="column">
                    {% if item.name %}
                    <a href="#" class="button">View Geneology</a>
                    {% endif %}
                </div>
            </div>
            {% endfor %}
            <div id="pages">
                <div id="page_info">Showing Pages {{current_page}} of {{num_pages}}</div>
                <div id="page">First</div>
                <div id="page">Prev</div>
                <div id="page">1</div>
                <div id="page">2</div>
                <div id="page">3</div>
                <div id="page">4</div>
                <div id="page">5</div>
                <div id="page">Next</div>
                <div id="page">Last</div>                
            </div>
        </div>
    </div>
</div>
{% endblock content %}