{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "admin/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Manage Users</h1>
    <div class="common_table_container">
        <div class="subcontent">
            <span class="subcontent_heading">Commissions</span>
            <br>
            <br>
            <div class="toprow">
                <div class="name">Customer</div>
                <div class="date">Order Date</div>
                <div class="name">Product</div>
                <div class="number">Price</div>
                <div class="number">Quantity</div>
                <div class="number">Discount</div>
                <div class="number">Total</div>                
                <div class="name">Label</div>
            </div>
            {% for item in users %}
            <div class="row">
                <div class="name">{{item.customer_name}}</div>
                <div class="date">{{item.order_datetime}}</div>
                <div class="name">{{item.price}}</div>
                <div class="number">{{item.quantity}}</div>
                <div class="number">{{item.discount}}</div>
                <div class="number">{{item.total}}</div>
                <div class="name">{{item.label}}</div>
            </div>
            {% endfor %}
            <div class="pagination">
                <div class="info">Showing Pages {{current_page}} of {{num_pages}}</div>
                <div class="pages">
                    <div class="item">
                        {% if current_page == first %}
                            <a href="#" class="page disable">First</a>
                        {% else %}
                            <a href="{{root}}admin/manage/page/{{first}}/user_type/{{user_type}}" class="page gradient">First</a>
                        {% endif %}
                    </div>
                    {% if current_page == first %}
                        <div class="item"><a href="#" class="page disable">Prev</a></div>
                    {% else %}
                        <div class="item"><a href="{{root}}admin/manage/page/{{prev}}/user_type/{{user_type}}" class="page gradient">Prev</a></div>
                    {% endif %}
                    {% for page in pages %}
                    <div class="item">
                        {% if page.show == 1 %}
                            {% if page.id == current_page %}
                                <a href="{{root}}admin/manage/page/{{page.id}}/user_type/{{user_type}}" class="page active">{{page.id}}</a>
                            {% else %}
                                <a href="{{root}}admin/manage/page/{{page.id}}/user_type/{{user_type}}" class="page gradient">{{page.id}}</a>
                            {% endif %}
                        {% else %}
                            <a href="#" class="page disable">{{page.id}}</a>
                        {% endif %}
                    </div>
                    {% endfor %}
                    {% if current_page == last %}
                        <div class="item"><a href="#" class="page disable">Next</a></div>
                        <div class="item"><a href="#" class="page disable">Last</a></div>   
                    {% else %}
                        <div class="item"><a href="{{root}}admin/manage/page/{{next}}/user_type/{{user_type}}" class="page gradient">Next</a></div>
                        <div class="item"><a href="{{root}}admin/manage/page/{{last}}/user_type/{{user_type}}" class="page gradient">Last</a></div>
                    {% endif %}
                </div>
            </div>            
        </div>
    </div>
    <div id="addnewuser_modal" class="modal_dialog">
        <div class="add_user_box">
            <a href="#close" title="Close" class="close">X</a>
            <br>
            <form action="{{root}}admin/add-user" method="post">
                <fieldset>
                    <div class="form_item">
                        <div><label>Username:</label></div>
                        <div><input type="text" class="text" name="user" value="" maxlength="16"/></div>
                    </div>
                    
                    <div class="form_item">
                        <div><label>Password:</label></div>
                        <div><input type="text" class="text" name="pass" value="" maxlength="32"/></div>                    
                    </div>
                        
                    <div class="form_item">
                        <div><label>Role:</label></div>
                        <div><select name="role">
                            <option value="1">Administrator</option>
                            <option value="2">Manager</option>
                            <option value="3">Sales</option>
                        </select></div>
                    </div>
                    
                    <div class="form_item">
                        <div><label>Fullname:</label></div>
                        <div><input type="text" class="text" name="fullname" value="" maxlength="128"/></div>   
                    </div>
                    
                    <div class="form_item">
                        <div><label>E-Mail:</label></div>
                        <div><input type="text" class="text" name="email" value="" maxlength="64"/></div>                                        
                    </div>
                    
                    <div class="form_item">
                        <button type="button" name="submit" class="form_button">Submit</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div id="searchuser_modal" class="modal_dialog">
        <div class="search_box">
            <a href="#close" title="Close" class="close">X</a>
            <br>
            <form action="{{root}}admin/manage/search" method="post">
                <fieldset>
                    <div id="field_item">
                        <label>Username:</label>
                        <input type="text" class="text" name="user" value="" maxlength="32"/>
                    </div>
                    <div id="field_item">
                        <button type="button" class="form_button">Search</button>
                    </div>                    
                </fieldset>
            </form>
        </div>
    </div>    
</div>
{% endblock content %}