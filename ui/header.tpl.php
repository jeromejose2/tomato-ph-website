<!DOCTYPE html>
<html>
<head>
    {% block head %}
    <title>{% block title %}{% endblock %}</title>

    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="Sphere Rewards Multi-Level Marketing">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="{{root}}css/normalize.css">
    <link rel="stylesheet/less" href="{{root}}css/main.less">
        <!--
        <link rel="stylesheet" href="{{root}}css/bootstrap.min.css">
        <link rel="stylesheet" href="{{root}}css/bootstrap-theme.min.css">
    -->
    <link rel="stylesheet" href="{{root}}css/common.css">
    <!-- <link rel="stylesheet" href="{{root}}css/ui-lightness/jquery-ui-1.10.3.custom.min.css"> -->

    <!-- <link rel="stylesheet" href="{{root}}css/fonts.css"> -->

    <!-- External Links -->
    <link href='http://fonts.googleapis.com/css?family=Fjalla+One&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,900' rel='stylesheet' type='text/css'>        
    <script language="JavaScript" type="text/javascript" src="{{root}}js/jquery-2.0.2.min.js"></script>
    {% endblock %}
</head>
<body>
    <div id="content">
        {% block content %}{% endblock %}
    </div>
    {% include "footer.tpl.php" %}

    <!--<script src="{{root}}js/jquery-1.9.1.min.js"></script>
        <!-- 
        <script src="{{root}}js/jquery-ui-1.10.3.custom.min.js"</script>
        <script src="{{root}}js/bootstrap.min.js"></script>
    -->
    
    <script src="{{root}}js/modernizr-2.6.2.min.js"></script>
    <script src="{{root}}js/less-1.4.1.min.js"></script>
    <script src="{{root}}js/json3.min.js"></script>
    <script src="{{root}}js/spin.min.js"></script>
    <script src="{{root}}js/plugins.js"></script>
    <script src="{{root}}js/sphere-0.1.js"></script>
</body>
</html>