{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "nav.tpl.php" %}
    <div id="cont-wrap">
        <br>
        <br>
        <br>
        <br>
        <div class="common_product_container">
            <h1 class="page-title">{{category}}</h1>            
            <div class="shopcart_left_container">
                {% for item in kits %}
                <div class="product_item" align="center">
                    <img src="{{root}}{{item.photo}}" style="width: 120px"/>
                    <br>
                    <br>
                    <a href="{{root}}">{{item.name}}</a><br>
                    <b>Price:</b> P {{item.price}}<br>
                    <input type="button" class="button" name="add_to_cart" value="Add To Cart"/>
                </div>
                {% endfor %}
            </div>
            <div class="shopcart_right_container">
                <input type="button" name="continue_shopping" value="Continue Shopping"/>
                <input type="button" name="view_cart" value="View Your Cart"/>
            </div>
        </div>
    </div>
{% endblock content %}