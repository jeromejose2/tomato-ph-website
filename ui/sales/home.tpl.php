{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "sales/nav.tpl.php" %}
        <div id="cont-wrap">
            <h1 class="page-title">DASHBOARD</h1>        
            <ul id="user-main-pane">
                <li>
                    <h2>Welcome back, {{name}}!</h2>
                    <p>Your last login was <em>{{last_login}}</em></p>
                </li>            
                <li>
                    <ul id="ump-opts">
                        <a href="{{root}}sales/changepassword"><li><strong>+</strong>Change Password</li></a>
                        <a href="{{root}}sales/editaccount"><li><strong>+</strong>Edit Account</li></a>
                        <a href="{{root}}sales/rewards-reports"><li><strong>+</strong>View Reports</li></a>
                    </ul>
                </li>
            </ul>        
        </div>
{% endblock content %}