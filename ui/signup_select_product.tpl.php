{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "nav.tpl.php" %}
    <div id="cont-wrap">
        <div id="members">
            <div class="subcontent">
                <span class="subcontent_heading">Select Product</span>
                <br>
                <br>
                {% for item in products %}
                <div class="row">
                    <div class="column" style="width: 260px;">
                        <div style=" background: #ffffff">
                            <img src="{{root}}images/{{item.photo}}" style="width: 120px"/>
                            <div>
                                <b>Item:</b> {{item.name}}<br>
                                <b>Price:</b> {{item.price}}<br>
                                <b>Description:</b> {{item.description}}<br>
                            </div>
                            <a href="{{root}}purchase-product/member_id/{{member_id}}/price/{{item.price}}" class="button">Buy</a>
                        </div>
                    </div>
                </div>
                {% endfor %}
            </div>
        </div>
    </div>
{% endblock content %}