{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "nav.tpl.php" %}
        <div id="cont-wrap">
            <br>
            <br>
            <br>
            <br>
            <span>
                Your account is not yet activated.<br>
                Please click the link below to resend your activation code.<br>
                <br>
                <a href="{{root}}resend">Re-send my activation code</a>
            </span>
        </div>
{% endblock content %}