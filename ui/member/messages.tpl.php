{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
{% include "member/nav.tpl.php" %}

<div id="cont-wrap">
	
	<h1 class="page-title">MESSAGES</h1>
	<br/>
	<br/>
	<br/>
	<div id="common_table_container">
		<div class="subcontent">
			<span class="subcontent_heading">Messages</span>
			<div class="dMsg">
				<table class="tMessage" width="80%">
					<br/>
					<br/>

					<thead><tr><th>DATE</th><th>FROM USER</th><th>MESSAGE</th><td></td></tr></thead>
					<tbody>
						{% for msg in message %}
						<tr align="center">
							<td class="tdDate">{{msg.datex}}</td>
							<td class="tdFrom"><img src="{{root}}{{msg.image_directory}}" width="40px" height="40px">{{msg.fullname}}</td>
							<td class="tdMsg">{{msg.message}}</td>
							<td class="tdReply"><a href="{{root}}members/compose/id/{{msg.fromx}}"><button class="form_button">REPLY</button></a></td>
						</tr>
						{% endfor %}
					</tbody>
				</table>

				<center><a href="{{root}}members/compose"><button class="form_button">CREATE NEW MESSAGE</button></a></center>


				<br/>
				<br/>
				<br/>
			</div>

		</div>
	</div>
</div>
{% endblock content %}