{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "member/nav.tpl.php" %}
<div id="cont-wrap">
    <br>
    <br>
    <br>
    <h1 class="page-title">Change Password</h1>
    <div class="common_table_container" align="center">
        <form action="{{root}}members/changepassword" method="post">
        <div class="form_item">
            <div class="form_label"><label>Old Password: </label></div>
            <div><input type="password" class="text" name="oldpass" maxlength="32" required></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>New Password: </label></div>
            <div><input type="password" class="text" name="newpass" maxlength="32" required></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Confirm Password: </label></div>
            <div><input type="password" class="text" name="confirmpass" maxlength="32" required></div>
        </div>
        <div class="form_item">
            <div><button type="submit" class="form_button">Update</button></div>
        </div>
        </form>    
    </div>    
</div>
{% endblock content %}