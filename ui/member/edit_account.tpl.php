{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "member/nav.tpl.php" %}
<div id="cont-wrap">
    <br>
    <br>
    <br>
    <h1 class="page-title">Edit Account Information</h1>
    <div class="common_table_container" align="center">
        <form action="{{root}}members/editaccount" method="post" enctype="multipart/form-data">
        <div class="form_item">
            <div class="form_label"><label>First Name: </label></div>
            <div><input type="text" class="text" name="firstname" value="{{firstname}}"></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Last Name: </label></div>
            <div><input type="text" class="text" name="lastname" value="{{lastname}}"></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>M.I.: </label></div>
            <div><input type="text" class="text" name="mi" value="{{mi}}"></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>E-mail: </label></div>
            <div><input type="text" class="text" name="email" value="{{email}}"></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Phone No.: </label></div>
            <div><input type="text" class="text" name="phone_no" value="{{phone_no}}"></div>
        </div>            
        <div class="form_item">
            <div class="form_label"><label>Mobile No.: </label></div>
            <div><input type="text" class="text" name="mobile_no" value="{{mobile_no}}"></div>
        </div>            
        <div class="form_item">
            <div class="form_label"><label>Street: </label></div>
            <div><input type="text" class="text" name="street" value="{{street}}"></div>
        </div>            
        <div class="form_item">
            <div class="form_label"><label>City: </label></div>
            <div><input type="text" class="text" name="city" value="{{city}}"></div>
        </div>            
        <div class="form_item">
            <div class="form_label"><label>State: </label></div>
            <div><input type="text" class="text" name="state" value="{{state}}"></div>
        </div>
        <div class="form_item">
            <div class="form_label"><label>Profile Picture </label></div>
            <div><input type="file" name="file" id="file"><br></div>
        </div> 

        <div class="form_item">
            <div><button type="submit" class="form_button">Update</button></div>
        </div>
        </form>    
    </div>    
</div>
{% endblock content %}