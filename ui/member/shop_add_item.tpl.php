{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "member/nav.tpl.php" %}
    <div id="cont-wrap">
        <br>
        <br>
        <br>
        <br>
        <div class="common_product_container">
            <div class="shopcart_category_container">
            </div>        
            <div class="shopcart_left_container">
                {% for item in product %}
                <div class="product-left-item">
                    <img src="{{root}}{{item.photo}}" class="product_photo"/>
                </div>
                <div class="product-right-item">
                    <div>
                        <span class="product-title">{{item.name}}</span><br>
                        <span class="product-description">{{item.description}}</span><br>
                        <br>
                        <b>Price:</b> &#x20b1; {{item.price}}<br>
                        <br>
                        <form action="{{root}}members/shop/action/add/id/{{item.id}}" method="post">
                            <fieldset>
                                <input type="hidden" name="discount" value="{{item.discount}}"/>
                                <input type="hidden" name="price" value="{{item.price}}"/>
                                <b>Quantity: </b><select id="qty" name="qty" class="quantity">
                                {% for n in item.qty %}
                                <option value="{{n}}">{{n}}</option>
                                {% endfor %}
                                </select>
                                <br>
                                <button type="submit" class="button">Add To Cart</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
                {% endfor %}    
            </div>
            <div class="shopcart_right_container">
                <button type="button" id="view_cart" class="form_button" value="{{root}}members/shop/action/viewcart">View Your Cart</button>
                <button type="button" id="continue_shopping" class="form_button" value="{{root}}members/shop">Continue Shopping</button>
            </div>
        </div>
    </div>
    <div id="root" style="display:none">{{root}}</div>
{% endblock content %}