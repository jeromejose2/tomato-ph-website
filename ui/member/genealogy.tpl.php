{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
{% include "member/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Member Genealogy</h1>
    <div id="members" style="overflow: auto;>"
        <div class="subcontent" >
            <span class="subcontent_heading">Genealogy</span>

            <br>
            <br>
            <ul id="user-accordion-pane" class="genelink">
                {% for item in members %}
                <li id="{{item.userID}}">
                    <a href="#" id="{{item.userID}}" class="accord">
                        <h3><strong class="accord"></strong> 
                            <img src="{{root}}{{item.image}}" width="40" height="40"/>
                            NAME {{item.name}} ID:{{item.userID}} Date Joined: {{item.date}} Points: {{item.points}}
                        </h3>
                    </a>
                    <div class="accopen {{item.userID}}">

 
                    </div>
                </li>
                {% endfor %}
            </ul>

            
        </div>
    </div>
</div>


<script language="JavaScript" type="text/javascript">
    $(document).ready(function(){
        $('ul#user-accordion-pane.genelink li a').on("click", function() {                     
            var clickedId =this.id.toString();
            $.post('{{root}}members/GenealogyDownline',{'userID': clickedId.toString()}, function(data){
                var myArray = jQuery.parseJSON(data);
                var displayAppend="";
                for(var i=0;i<myArray.length;i++){
                    var aID=myArray[i].id;
                    var aImage=myArray[i].image;
                    var aDate = myArray[i].date;
                    var aPoints = myArray[i].points;
                    var aName = myArray[i].name;
                    displayAppend+="<li ><a href='#' id='"+aID+"' class='accord'><h3><strong class='accord'></strong>"
                    +"<img src='{{root}}"+aImage+"' width='40' height='40'/>NAME "+aName+" ID: "+aID+"  Date Joined: "+aDate+"  Points: "+aPoints+" </h3></a>"
                    +"<div class= 'accopen "+aID+"'></div></li>";
                }
                displayAppend = "<ul id='user-accordion-pane' class='genelink2'>"+displayAppend+"</ul>";
                var appendLoc='div.'+clickedId+'.accopen';
                $(appendLoc).html('').append(displayAppend);

            });                        
    });
   
});
$(document).on('click','ul#user-accordion-pane.genelink2 li a', function(){
    var clickedId =this.id.toString();
    var accordion = 'div.' + this.id + '.accopen';
    var plus = 'ul li a#' + this.id; 
    $(accordion).toggle();
    $(plus).addClass('open');
    
    if ($('.'+this.id).is(':visible')) {
        $(plus).addClass('open');
    } else {
        $(plus).removeClass('open');
    }
   
    $.post('{{root}}members/GenealogyDownline',{'userID': clickedId.toString()}, function(data){
                var myArray = jQuery.parseJSON(data);
                var displayAppend="";
                for(var i=0;i<myArray.length;i++){
                    var aID=myArray[i].id;
                    var aImage=myArray[i].image;
                    var aDate = myArray[i].date;
                    var aPoints = myArray[i].points;
                    var aName = myArray[i].name;
                    displayAppend+="<li ><a href='#' id='"+aID+"' class='accord'><h3><strong class='accord'></strong>"
                    +"<img src='{{root}}"+aImage+"' width='40' height='40'/>NAME "+aName+" ID: "+aID+"  Date Joined: "+aDate+"  Points: "+aPoints+" </h3></a>"
                    +"<div class= 'accopen "+aID+"' ></div></li>";
                }

                displayAppend = "<ul id='user-accordion-pane' class='genelink2'>"+displayAppend+"</ul>";
                var appendLoc='div.'+clickedId+'.accopen';
                
                  
                    $(appendLoc).html('').append(displayAppend);
               
               
            });

});


</script>
{% endblock content %}