{% extends "index.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include 'member_nav.tpl.php' %}
    <div id="cont-wrap">
        <h1 class="page-title">User Info</h1><br />
        <table border = 0 class = 'hovertable' align = 'center'>
            <tr><td colspan = 2 align='center'><img src ='{{root}}photo/{{photo}}' width = '270' height = '180'></td><br /></tr>
            <tr><td>Name:</td><td>{{member_name}}</td></tr>
            <tr><td>Address:</td><td>{{address}}</td></tr>
            <tr><td>Contact No:</td><td>{{landline_no}}</td></tr>
            <tr><td>E-mail Address:</td><td>{{email}}</td></tr>
            <tr><td align = 'center'colspan = 2><p><a href='#'><input type="reset" name="reset" value="EDIT PROFILE"></a></p></td></tr>
        </table>
    </div>
{% endblock content %}