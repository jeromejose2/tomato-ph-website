{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "member/nav.tpl.php" %}
    <div id="cont-wrap">
        <br>
        <br>
        <br>
        <br>
        <div class="common_product_container">
            <div class="shopcart_left_container">
                <div>
                    <div class="order_row">
                        <div class="qty_top">Quantity</div>
                        <div class="name_top">Description</div>
                        <div class="price_top">Price</div>
                    </div>
                    {% for item in cart_items %}
                    <div class="order_row">
                        <div class="qty">{{item.quantity}}</div>
                        <div class="name">{{item.name}}</div>
                        <div class="price">&#x20b1; {{item.price}}</div>
                    </div>
                    {% endfor %}
                    <div class="order_row">
                        <div class="qty_last">Total</div>
                        <div class="name_last">&nbsp;</div>
                        <div class="price_last">&#x20b1; {{total}}</div>
                    </div>                    
                </div>
            </div>
            <div class="shopcart_right_container">
                <button type="button" id="checkout" class="form_button" value="{{root}}members/shop/action/checkout">Check Out</button>
                <button type="button" id="continue_shopping" class="form_button" value="{{root}}members/shop">Continue Shopping</button>
            </div>
        </div>
    </div>
{% endblock content %}