{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
{% include "member/nav.tpl.php" %}	
<?php
 //    $myurl = $_SERVER['QUERY_STRING'];
	// echo $myurl;
	// $myArray = explode('=', $myurl);
	// $value = $myArray[2];
	// echo $value; 
?>
<div id="cont-wrap">
    <br>
    <br>
    <br>
    <h1 class="page-title">Compose Message</h1>
    <div class="common_table_container" align="center">
        <form action="{{root}}members/compose/action/post" method="post">
            <div class="form_item">
                <div class="form_label">
                    <label>To: 

                    </label>
                </div>
                <div class="form_item">
                    <div><select name='to' class="text">
                        {% for usr in rsUser %}
                        <option value="{{usr.id}}">{{usr.fullname}}</option>
                        {% endfor%}
                    </select></div>
                </div>
            </div>
            <div class="form_item">
                <div class="form_label"><label>Message </label></div>
                <div><input type="input" class="text" name="message" ID="message" maxlength="32" required></div>
            </div>

            <div class="form_item">
                <div><button type="submit" class="form_button">Send</button></div>
            </div>
        </form>    
    </div>    
</div>
{% endblock content %}