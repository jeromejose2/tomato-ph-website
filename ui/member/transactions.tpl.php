{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include 'member/nav.tpl.php' %}
<div id="cont-wrap">
    <h1 class="page-title">Transactions</h1>
    <br>
    <br>
    <br>
    <br>
    <div id="common_table_container">
        <div class="subcontent">
            <span class="subcontent_heading">Purchased Products</span>
            <br>
            <br>
            <div class="toprow">
                <div class="number">Id</div>
                <div class="date">Date</div>
                <div class="name">Product</div>
                <div class="name">Type</div>
                <div class="number">Price</div>
                <div class="number">Quantity</div>
                <div class="number">Total</div>
                <div class="number">Sphere Points</div>
            </div>
            {% for item in transactions %}
            {% set foo =  item.total  %}
            <div class="row">
                <div class="number">{{item.id}}</div>
                <div class="date">{{item.order_datetime}}</div>
                <div class="name">{{item.product}}</div>
                <div class="name">{{item.label}}</div>
                <div class="number">{{item.price}}</div>
                <div class="number">{{item.quantity}}</div>
                <div class="number">{{item.total}}</div>
                <div class="number">{{ foo|replace({(',') : '' }) / 5 }}</div>
            </div>
            {% endfor %}
            <div class="pagination">
                <div class="info">Showing Pages {{current_page}} of {{num_pages}}</div>
                <div class="pages">
                    <div class="item">
                        {% if current_page == first or prev == first %}
                            <a href="#" class="page disable">First</a>
                        {% else %}
                            <a href="{{root}}members/transactions/page/{{first}}" class="page gradient">First</a>
                        {% endif %}
                    </div>
                    {% if current_page == first or prev == first %}
                        <div class="item"><a href="#" class="page disable">Prev</a></div>
                    {% else %}
                        <div class="item"><a href="{{root}}members/transactions/page/{{prev}}" class="page gradient">Prev</a></div>
                    {% endif %}
                    {% for page in pages %}
                    <div class="item">
                        {% if page.show == 1 %}
                            {% if page.id == current_page %}
                                <a href="{{root}}members/transactions/page/{{page.id}}" class="page active">{{page.id}}</a>
                            {% else %}
                                <a href="{{root}}members/transactions/page/{{page.id}}" class="page gradient">{{page.id}}</a>
                            {% endif %}
                        {% else %}
                            <a href="#" class="page disable">{{page.id}}</a>
                        {% endif %}
                    </div>
                    {% endfor %}
                    {% if current_page == last or next == last %}
                        <div class="item"><a href="#" class="page disable">Next</a></div>
                        <div class="item"><a href="#" class="page disable">Last</a></div>   
                    {% else %}
                        <div class="item"><a href="{{root}}members/transactions/page/{{next}}" class="page gradient">Next</a></div>
                        <div class="item"><a href="{{root}}members/transactions/page/{{last}}" class="page gradient">Last</a></div>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
</div>
{% endblock content %}