{% extends "index.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include 'member_nav.tpl.php' %}
<div id="cont-wrap">
	<h1 class="page-title">My Income</h1>
	<table class='hovertable' align ='center'>
		<td><b>Search level</b></td>
		<td colspan = 5 align = "center">
			<input name="search" type="text" maxlength="512" id="search" class="searchField" autocomplete="off" title="" />
			<input type="submit" name="btnSearch" value="Search" id=" btnSearch" class="buttonSearch" />
		</td>
        <tr><input type="hidden" name="command" value="searchgen"></tr>
	</table>		
	<table class='hovertable' align='center'>
		<th colspan='5'>Enrollees</th>
		<tr>
			<td>
				<a href="income" style='text-decoration:none;color:black;'>Level 1 (DIRECT)</a>
			</td>
            <td>&nbsp;<strong>|</strong>&nbsp;</td>
			<td> 
				<a href="income" style='text-decoration:none;color:black;'>Level 2</a> 
			</td>
            <td>&nbsp;<strong>|</strong>&nbsp;</td>
			<td>
				<a href="income"style='text-decoration:none;color:black;'>Level 3</a>
			</td>
            <td>&nbsp;<strong>|</strong>&nbsp;</td>
			<td>
				<a href="income"style='text-decoration:none;color:black;'>Level 4</a>
			</td>
            <td>&nbsp;<strong>|</strong>&nbsp;</td>
			<td>
				<a href="income"style='text-decoration:none;color:black;'>Level 5</a>
			</td>
		</tr>
	</table>
	<br />
	<table class='hovertable' align='center'>
		<tr><th> Income </th><th>View Details</th></tr>
		<tr>
			<td><input type="text" name="income" class="text" id="sum" size="10" value="{{income}}" style="text-align:center" readonly></td>
			<td style = 'overflow:hidden; width:100px' align = 'center'>
				<a href="#">
				<img src="{{root}}icons/browse.png"></a>
			</td>
		</tr>
	</table>
	<br>
	<table width="70%" cellspacing="2" cellpadding="2" align="center" style="border:1px #000000 solid;" class="hovertable">
		<tr>
            <th>&nbsp;</th>
			<th>Enrollee</th>
			<th>Account ID</th>
			<th>Joined</th>
			<th>Sponsored By</th>
		</tr>
        {% for item in downlines %}
        <tr>
            <td style="text-align:right">{{item.index}}.</td>
            <td style="text-align:left">{{item.member_name}}</td>
            <td style="text-align:right">{{item.user_id}}</td>
            <td style="text-align:center">{{item.date_created}}</td>
            <td style="text-align:left">{{item.sponsor_id}}</td> 
        </tr>
        {% endfor %}
	</table>
	<br/>
	<table width="100%" cellspacing="2" cellpadding="2" align="center" >
		<tr><td align="center">
		</td></tr>
	</table>
</div>
{% endblock content %}