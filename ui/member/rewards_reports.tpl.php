{% extends "header.tpl.php" %}
{% block title %}{{pageTitle}}{% endblock %}

{% block content %}
        {% include "member/nav.tpl.php" %}
<div id="cont-wrap">
    <h1 class="page-title">Reports</h1>
    <br>
    <br>
    <br>
    <div class="order_detail_container" align="center">
        <div class="form_item">
            <div class="form_label"><label>Total Orders (YTD):</label></div>
            <div><input type="text" class="text" value="{{total}}" readonly></div>
        </div>
        <div class="form_item">
            <div class="">&nbsp;</div>
            <div class="common_table_container">
                <div class="subcontent">
                    <span class="subcontent_heading">Weekly Sales for the Month of {{current_month}}</span>
                    <br>
                    <br>
                    <div class="toprow">
                        <div class="topcolumn"><b>Week</b></div>
                        <div class="topcolumn"><b>Total Price</b></div>
                        <div class="topcolumn"><b>Total Items</b></div>
                        <div class="topcolumn"><b>&nbsp;</b></div>
                    </div>
                    {% for m in current_month_sales %}
                    <div class="row">
                        <div class="column">{{m.week}}</div>
                        <div class="column">&#x20b1; {{m.total_price}}</div>
                        <div class="column">{{m.total_items}}</div>
                        <div class="column">&nbsp;</div>
                    </div>
                    {% endfor %}
                    <div id="pages" style="text-align: right;"><h2>Total &#x20b1; {{total_sales}}</h2></div>                    
                </div>
            </div>
        </div>
    </div>
</div>
{% endblock content %}