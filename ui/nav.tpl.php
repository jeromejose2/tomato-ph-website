        <header>
            <div class="navigation main-nav">
                <ul id="main-nav">
                    <a href="http://sphere.ph/tomato"><li class="top tptomato"></li></a>
                    <a href="http://sphere.ph/time"><li class="top tptime"></li></a>
                    <a href="http://sphere.ph/swap"><li class="top tpswap"></li></a>
                    <a href="http://sphere.ph"><li>Join Sphere Now!</li></a>
                    {% if show_cart_items %}
                    <a href="#"><li class="tpitems"><strong class="cart-ico"></strong>Items in Cart (0)</li></a>
                    {% endif %}
                </ul>
            </div>
            <div class="navigation sub-nav">
            	<ul id="sub-nav">
                    <div id="banner">
                        <h1 class="web-ban"></h1>
                    </div>
                    <li><a href="{{root}}home">Home</a></li>
                	<li id="learn-more">
                        <a href="#">Learn More</a>
                    	<ul class="sublets learn-more">
                        	<li><a href="#">Terminology</a></li>
                        	<li><a href="#">Downloadables</a></li>
                        	<li><a href="#">Videos</a></li>
                        	<li><a href="#">News and Events</a></li>
                        	<li><a href="#">Features</a></li>
                        	<li><a href="#">Testimonials</a></li>
                        	<li><a href="#">Training Schedules</a></li>
                        </ul>
                    </li> 
                    <li><a href="{{root}}signup">Sign Up Now</a></li>
                    <li><a href="{{root}}home">Sign-In</a></li>
                    {% if logout %}
                	<li id="logout"><a href="{{root}}logout">Logout</a></li>
                    {% endif %}
                </ul>
            </div>            
        </header>
