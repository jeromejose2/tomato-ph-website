<?php
 /*
  * Developed by Cris del Rosario
  */

 define ('ROOT',($_SERVER['SERVER_NAME'] == "localhost") ? "http://localhost/rewards/" : "http://rewards.sphere.ph/");
 $MODELS = array(
     // Home
     'home' => array(
        'Home',
        'SignUp',
        'SignIn',
        'Logout',
        'Register',
        'Signup_Select_Kit',
        'Purchase_Kit',
        'Signup_Select_Product',
        'Purchase_Product',
        'Verify_Account',
        'Verify',
        'Complete',
        'Activate', 
        'Shop',
        'Payment_Method',
        'Error',
        'Resend'
     ),
     
     // Help
    'help' => array(
        'help/Contact_Us',
        'help/Store_Locator',
        'help/Size_Charts',
        'help/FAQ',
        'help/How_To_Order',
        'help/Payment_Instructions',
        'help/Return_Policy',
        'help/Shipping_Advisory',
        'help/Nationwide_Destination',
        'help/Worldwide_Shipping',
        'help/Terms_And_Condition'
    ),
     
    // Brands
    'brands' => array(
        'brands/Tomato',
        'brands/Tomato_Time',
        'brands/Swap'
    ),    
     
    // Recruitment & Incentives
    'recruitment' => array(
        'recruitment/How_To_Earn',
        'recruitment/Commissions',
        'recruitment/Incentives',
        'recruitment/Videos'
    ),
     
    // Sphere    
    'sphere' => array(
        'sphere/About',
        'sphere/Work_With_User',
        'sphere/Blog'
    ),
     
    // Members
    'members' => array( 
        'members/Home',
        'members/Account',
        'members/Income',
        'members/Transactions',
        'members/MyShop',
        'members/OnlineShop',
        'members/MembersShop',
        'members/Genealogy',
        'members/Rewards_Reports',
        'members/ChangePassword',
        'members/Shop',
        'members/Account_Settings',
        'members/EditAccount',
        'members/Commissions',
		'members/Messages',
		'members/GenealogyDownline',
		'members/compose'
    ),
    
    // Admin
    'admin' => array(
        'admin/Home',
        'admin/Members',
        'admin/Manage',
        'admin/Member_Genealogy',
        'admin/Products',
        'admin/Transactions',
        'admin/Purchase',
        'admin/Registration',
        'admin/OnlineShop',
        'admin/Search_User',
        'admin/Add_User',
        'admin/Add_User_Success',
        'admin/Sphere_Reports',
        'admin/Rewards_Reports',
        'admin/Add_Product',
        'admin/Account_Settings', 
        'admin/MemberInfo',
        'admin/Orders',
        'admin/Commissions'
    ),
     
    'manager' => array(
        'manager/Home'
    ),
     
    'sales' => array(
        'sales/Home'
    ),
    
    // API
    'api' => array( 
        'api/Compute_Commissions',
        'api/test'
    )
 ); 
?>